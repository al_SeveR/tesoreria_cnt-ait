void crea_menu(Gtk.ApplicationWindow window, Gtk.Popover pop, Gtk.Button aux_button2)
{				
	// Se crea una caja y se añade al popover
	Gtk.Box boxpop = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
	boxpop.set_property("margin", 10);
	pop.add(boxpop);
				
	//Se añaden elementos a la caja del popover				
	Gtk.ModelButton quotes = new Gtk.ModelButton();
	quotes.set_property("text", "Cotizaciones");
	boxpop.pack_start(quotes);
				
	Gtk.ModelButton backup = new Gtk.ModelButton();
	backup.set_property("text", "Copia de seguridad");
	boxpop.pack_start(backup);
    
    Gtk.ModelButton preferences = new Gtk.ModelButton();
	preferences.set_property("text", "Preferencias");
	boxpop.pack_start(preferences);
				
	Gtk.ModelButton help = new Gtk.ModelButton();
	help.set_property("text", "Ayuda");
	boxpop.pack_start(help);
				
	Gtk.ModelButton about = new Gtk.ModelButton();
	about.set_property("text", "Acerca de");
	boxpop.pack_start(about);
				
	// Se conectan las señales con las funciones
	quotes.clicked.connect ( () =>{cotizaciones(window);});
	backup.clicked.connect ( () =>{copia(window);});
    preferences.clicked.connect ( () =>{preferencias(window, aux_button2);});
	about.clicked.connect ( () =>{acerca_de(window);});
	help.clicked.connect ( () =>{ayuda(window);});
}

void cotizaciones (Gtk.ApplicationWindow window)
{
	Gtk.Dialog dialog = new Gtk.Dialog.with_buttons("Cotizaciones", window, Gtk.DialogFlags.MODAL, "Cerrar", Gtk.ResponseType.CLOSE);
	dialog.set_resizable(false);
	var content_area = dialog.get_content_area();
	content_area.set_property("margin", 10);
	content_area.set_property("spacing", 10);
	
	Gtk.Box vbox = new Gtk.Box(Gtk.Orientation.VERTICAL, 10);
	Gtk.Box hbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
	hbox.halign = Gtk.Align.CENTER;
	
	Gtk.Label label_year = new Gtk.Label("Año:");
	hbox.pack_start(label_year, false, false, 0);	
	Gtk.ListStore list = new Gtk.ListStore(1, typeof(string));
	Gtk.TreeIter iter;	
	list.append(out iter);
	int i = 0;
	db.exec(@"SELECT anho FROM cotizaciones", (n_columns, values, column_names) => 
	{
		i++;
		list.set(iter, 0, values[0]);
		list.append(out iter);
		return 0;
	}, null);
	if (i > 0)
	{
		list.remove(ref iter);
		Gtk.ComboBox combo = new Gtk.ComboBox.with_model(list);
		Gtk.CellRendererText cell = new Gtk.CellRendererText ();
		combo.pack_start(cell,false);
		combo.set_attributes (cell, "text", 0);
		var settings = new Settings ("cnt.tesoreria");
		var sett_cot = settings.get_int ("anho");
		if(sett_cot < i)
		{
			combo.set_active(sett_cot);
		}
		else
		{
			combo.set_active(i-1);
		}
		hbox.pack_start(combo, false, false, 0);
		
		Gtk.ListStore list_months = new Gtk.ListStore (12, typeof(string), typeof(string), typeof(string),typeof(string), typeof(string), typeof(string), typeof(string), typeof(string), typeof(string),typeof(string), typeof(string), typeof(string));
		Gtk.TreeView view = new Gtk.TreeView.with_model(list_months);
		var selection = view.get_selection();
		selection.set_mode(Gtk.SelectionMode.NONE);
			
		view.insert_column_with_attributes (-1, "Enero", cell, "text", 0);
		var col = view.get_column(0);
		view.insert_column_with_attributes (-1, "Febrero", cell, "text", 1);
		col = view.get_column(1);
		view.insert_column_with_attributes (-1, "Marzo", cell, "text", 2);
		col = view.get_column(2);
		view.insert_column_with_attributes (-1, "Abril", cell, "text", 3);
		col = view.get_column(3);	
		view.insert_column_with_attributes (-1, "Mayo", cell, "text", 4);
		col = view.get_column(4);	
		view.insert_column_with_attributes (-1, "Junio", cell, "text", 5);	
		col = view.get_column(5);
		view.insert_column_with_attributes (-1, "Julio", cell, "text", 6);
		col = view.get_column(0);
		view.insert_column_with_attributes (-1, "Agosto", cell, "text", 7);
		col = view.get_column(1);
		view.insert_column_with_attributes (-1, "Septiembre", cell, "text", 8);
		col = view.get_column(2);
		view.insert_column_with_attributes (-1, "Octubre", cell, "text", 9);
		col = view.get_column(3);	
		view.insert_column_with_attributes (-1, "Noviembre", cell, "text", 10);
		col = view.get_column(4);	
		view.insert_column_with_attributes (-1, "Diciembre", cell, "text", 11);	
		col = view.get_column(5);
	
		vbox.pack_start(hbox);
		vbox.pack_start(view);
		
		crea_lista_meses(list_months, combo);
		combo.changed.connect (() =>
		{
			crea_lista_meses(list_months, combo);
			settings.set_int ("anho", combo.get_active());
		});
	}
	else
	{
		label_year.set_text("Aún no se han realizado cotizaciones");
		vbox.pack_start(hbox);
	}
	content_area.add(vbox);
	content_area.show_all();
	
	dialog.run();
	dialog.destroy();	
}

void copia (Gtk.ApplicationWindow window)
{
	var dialog = new Gtk.FileChooserDialog("Guardar como...", window, Gtk.FileChooserAction.SAVE, "Cancelar", Gtk.ResponseType.CANCEL, "Guardar", Gtk.ResponseType.OK);
	dialog.set_modal(true);
	dialog.set_do_overwrite_confirmation(true);
	var now = new DateTime.now_local();
	var date = now.format("%Y-%m-%d");
	dialog.set_current_name(@"$(date)_backup_tesoreria.db");
	var result = dialog.run();
	if(result == Gtk.ResponseType.OK) 
	{
		string home = GLib.Environment.get_home_dir();
		string path = home + "/.local/share/tesoreria/tesoreria.db";
	
		var file_orig = File.new_for_path (path);
		var file = dialog.get_file();
		try
		{
			if(file.query_exists() && file.get_path() != file_orig.get_path())
			{
				file.delete();
			}
			file_orig.copy(file, 0, null, null);
		}
		catch(Error e1)
		{
			stderr.printf("%s\n", e1.message);
		}
	}	
	dialog.destroy();	
}

void acerca_de (Gtk.ApplicationWindow window)
{
	// Se muestra el diálogo
	string[] authors = {"Cristian (SEIS Madrid)"};
	string[] documenters = {"Alfonso (SOV Ponent)"};
	Gtk.show_about_dialog (window,
	                       "program-name", "Tesorería CNT-AIT",
	                       "version", "1.2.3",
	                       "copyright", "CNT-AIT",
	                       "authors", authors,
	                       "documenters", documenters,
	                       "website", "https://gitlab.com/al_SeveR/tesoreria_cnt-ait",
	                       "website-label", "GitLab del proyecto",
	                       "license-type", Gtk.License.GPL_3_0,
	                       "logo-icon-name", "tesoreria"
	                       );
}

void ayuda (Gtk.ApplicationWindow window)
{
	//Se llama a una url para que se abra en el navegador
	try
	{
		Gtk.show_uri_on_window(window, "file:///usr/share/doc/tesoreria/ayuda.html", 0);
	}
	catch(Error e1)
	{
		stderr.printf("Error: %s\n", e1.message);
	}
}

void crea_lista_meses(Gtk.ListStore list, Gtk.ComboBox combo)
{
	Gtk.TreeIter iter;
	combo.get_active_iter(out iter);
	var model = combo.get_model();
	Value anho_v;
	model.get_value(iter, 0, out anho_v);
	var anho = (string) anho_v;
	
	list.clear();
	list.prepend(out iter);
		
	db.exec(@"SELECT * FROM cotizaciones WHERE anho = '$anho'", (n_columns, values, column_names) => 
	{	
		string january = values[1];
		string february = values[2];
		string march = values[3];
		string april = values[4];		
		string may = values[5];
		string june = values[6];			
		string july = values[7];
		string august = values[8];
		string september = values[9];
		string october = values[10];		
		string november = values[11];
		string december = values[12];
					
		list.set(iter, 0, january, 1, february, 2, march, 3, april, 4, may, 5, june, 6, july, 7, august, 8, september, 9, october, 10, november, 11, december);
		list.prepend(out iter);
			
		return 0;
	}, null);	
	list.remove(ref iter);
}

void preferencias (Gtk.ApplicationWindow window, Gtk.Button aux_button2)
{
	Gtk.Dialog dialog = new Gtk.Dialog.with_buttons("Preferencias", window, Gtk.DialogFlags.MODAL, "Cerrar", Gtk.ResponseType.CLOSE);
	dialog.set_resizable(false);
	var content_area = dialog.get_content_area();
	content_area.set_property("margin", 10);
	content_area.set_property("spacing", 10);
	
	Gtk.Grid grid = new Gtk.Grid();
	grid.set_column_spacing(10);
	grid.set_row_spacing(10);
	
	Gtk.Label label_buttons = new Gtk.Label("Botones");
    label_buttons.set_markup("<b>Botones</b>");
	label_buttons.halign = Gtk.Align.START;
	grid.attach(label_buttons, 0, 0, 1, 1);
    
    Gtk.RadioButton r_menu = new Gtk.RadioButton.with_label_from_widget (null, "Menú opciones");
    grid.attach(r_menu, 0, 1, 1, 1);
    
	Gtk.RadioButton r_buttons = new Gtk.RadioButton.with_label_from_widget (r_menu, "Botones individuales");
    grid.attach(r_buttons, 0, 2, 1, 1);
    
    // Se toman del schema el valor botones
	var settings = new Settings ("cnt.tesoreria");
	var botones = settings.get_boolean ("botones");
    if(botones == true)
    {
        r_buttons.set_active(true);
    }
    
    r_menu.toggled.connect (() =>
	{
		if(r_menu.get_active())
        {
            aux_button2.clicked();
            botones = settings.get_boolean ("botones");
            settings.set_boolean("botones", !botones);
        }
	});
    r_buttons.toggled.connect (() =>
	{
		if(r_buttons.get_active())
        {
            aux_button2.clicked();
            botones = settings.get_boolean ("botones");
            settings.set_boolean("botones", !botones);
        }
	});
	
	content_area.add(grid);
	content_area.show_all();
	
	dialog.run();
	dialog.destroy();	
}
