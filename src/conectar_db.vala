//Globales
int cajas = 0;
Sqlite.Database db;

void conectar_db()
{
	int rc;

	string home = GLib.Environment.get_home_dir();
	string path = home + "/.local/share/tesoreria";
		
	// Referencia a la carpeta
    var file = File.new_for_path (path);
    
	// Se comprueba si existe la carpeta
    if (!file.query_exists ()) 
    {
		//Se crea la carpeta
		try
		{
			file.make_directory();
		}
		catch(Error e)
		{
			stderr.printf("%s\n", e.message);
		}
    }
    path = path + "/tesoreria.db";
    file = File.new_for_path (path);    
    
	// Se comprueba si existe el archivo
    if (!file.query_exists ()) 
    {
		// Se crea y conecta a la base de datos
		if ((rc = Sqlite.Database.open (path, out db)) != Sqlite.OK) 
		{
            stderr.printf ("Can't open database: %s\n", db.errmsg ());
        }
        
        // Se crean las tablas
        db.exec ("CREATE TABLE altas (cc text, nombre text, apellido1 text, apellido2 text, fnacimiento date, telefono text, email text, dir text, zona text, obs text, ocu text, sec text, centro text, dirc text, obsc text, afiliado date, cotizado date)", null, null);
		db.exec ("CREATE TABLE bajas (cc text, nombre text, apellido1 text, apellido2 text, fnacimiento date, telefono text, email text, dir text, zona text, obs text, ocu text, sec text, centro text, dirc text, obsc text, afiliado date, cotizado date, fechabaja date)", null, null);
        db.exec ("CREATE TABLE cotizaciones (anho int, enero int, febrero int, marzo int, abril int, mayo int, junio int, julio int, agosto int, septiembre int, octubre int, noviembre int, diciembre int)", null, null);	
    }
    else
    {
		// Se conecta a la base de datos
		if ((rc = Sqlite.Database.open (path, out db)) != Sqlite.OK) 
		{
            stderr.printf ("Can't open database: %s\n", db.errmsg ());
        }
        
        // Se cuentan las cajas existentes
        db.exec("SELECT count(*) FROM sqlite_master WHERE type = 'table' AND name != 'altas' AND name != 'bajas' AND name != 'cotizaciones'", (n_columns, values, column_names) => 
        {
			cajas = int.parse(values[0]);
            return 0;
        }, null);
	}
}
