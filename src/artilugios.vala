//Globales
string filters_s;
int months_fil = 6;
string search_text;
bool first_flag_af = true;
bool first_flag_ca = true;

void artilugios_af (Gtk.Stack stack, Gtk.ApplicationWindow window, Gtk.Button aux_button, Gtk.Button aux_button2, Gtk.SearchEntry search)
{
	filters_s = "all";
	search_text = "";
	
	Gtk.Box vbox = new Gtk.Box(Gtk.Orientation.VERTICAL, 10);
	Gtk.Box hbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			
	Gtk.ListStore list = new Gtk.ListStore(1, typeof(string));
	Gtk.TreeIter iter;
	list.append(out iter);
	list.set(iter, 0, "Altas");
	list.append(out iter);
	list.set(iter, 0, "Bajas");	
	Gtk.ComboBox combo_af = new Gtk.ComboBox.with_model(list);
	Gtk.CellRendererText cell = new Gtk.CellRendererText ();
	combo_af.pack_start(cell,false);
	combo_af.set_attributes (cell, "text", 0);
    
    // Se toma del schema el número de tabla y se selecciona
	var settings = new Settings ("cnt.tesoreria");
    var sett_altas_bajas = settings.get_int ("altas-bajas");
	combo_af.set_active(sett_altas_bajas);
	combo_af.set_tooltip_text("Seleccionar altas/bajas");
	hbox.pack_start(combo_af, true, false, 0);	
	
	Gtk.MenuButton showbutton = new Gtk.MenuButton();
	showbutton.set_label("Mostrar");
	Gtk.Popover pop = new Gtk.Popover(showbutton);
	showbutton.set_popover(pop);
	showbutton.set_tooltip_text("Seleccionar columnas visibles");
	hbox.pack_start(showbutton, true, false, 0);	

	Gtk.MenuButton filters = new Gtk.MenuButton();
	filters.set_label("Filtros");
	Gtk.Popover pop_fil = new Gtk.Popover(filters);
	filters.set_popover(pop_fil);
	filters.set_tooltip_text("Seleccionar filtro");	
	hbox.pack_start(filters, true, false, 0);

	vbox.pack_start(hbox, false, false, 0);
	
	Gtk.ListStore list_af = new Gtk.ListStore (19, typeof(string), typeof(string), typeof(string),typeof(string), typeof(string), typeof(string), typeof(string), typeof(string),typeof(string), typeof(string), typeof(string), typeof(string), typeof(string),typeof(string), typeof(string), typeof(string), typeof(string), typeof(string), typeof(string));
	Gtk.TreeView view = new Gtk.TreeView.with_model(list_af);
	view.set_enable_search(false);
	var selection = view.get_selection();
	
	// Se toman del schema los valores de los check
	settings = new Settings ("cnt.tesoreria.mostrar");
	var num_check = settings.get_boolean ("numero");
	var cc_check = settings.get_boolean ("cc");
	var name_check = settings.get_boolean ("nombre");
	var surname1_check = settings.get_boolean ("apellido1");
	var surname2_check = settings.get_boolean ("apellido2");
	var birth_check = settings.get_boolean ("fnacimiento");
	var phone_check = settings.get_boolean ("telefono");
	var email_check = settings.get_boolean ("email");
	var address_check = settings.get_boolean ("dir");
	var zone_check = settings.get_boolean ("zona");
	var obs_check = settings.get_boolean ("obs");
	var ocu_check = settings.get_boolean ("ocu");
	var sec_check = settings.get_boolean ("sec");
	var center_check = settings.get_boolean ("centro");
	var address_c_check = settings.get_boolean ("dirc");
	var obs_c_check = settings.get_boolean ("obsc");
	var date_check = settings.get_boolean ("afiliado");
	var cot_check = settings.get_boolean ("cotizado");
	var out_check = settings.get_boolean ("fechabaja");

	view.insert_column_with_attributes (-1, "Número", cell, "text", 0);
	var col = view.get_column(0);
	col.set_resizable(true);
	col.set_visible(num_check);
	col.set_sort_column_id(0);
	view.insert_column_with_attributes (-1, "CC", cell, "text", 1);
	col= view.get_column(1);
	col.set_resizable(true);
	col.set_visible(cc_check);
	col.set_sort_column_id(1);
	view.insert_column_with_attributes (-1, "Primer apellido", cell, "text", 2);
	col = view.get_column(2);
	col.set_resizable(true);
	col.set_visible(surname1_check);
	col.set_sort_column_id(2);
	view.insert_column_with_attributes (-1, "Segundo apellido", cell, "text", 3);
	col = view.get_column(3);
	col.set_resizable(true);
	col.set_visible(surname2_check);
	col.set_sort_column_id(3);
	view.insert_column_with_attributes (-1, "Nombre", cell, "text", 4);
	col = view.get_column(4);
	col.set_resizable(true);
	col.set_visible(name_check);
	col.set_sort_column_id(4);	
	view.insert_column_with_attributes (-1, "Fecha de nacimiento", cell, "text", 5);	
	col = view.get_column(5);
	col.set_resizable(true);
	col.set_visible(birth_check);
	col.set_sort_column_id(5);
	view.insert_column_with_attributes (-1, "Teléfono", cell, "text", 6);
	col = view.get_column(6);
	col.set_resizable(true);
	col.set_visible(phone_check);
	col.set_sort_column_id(6);
	view.insert_column_with_attributes (-1, "e-mail", cell, "text", 7);
	col = view.get_column(7);
	col.set_resizable(true);
	col.set_visible(email_check);
	col.set_sort_column_id(7);
	view.insert_column_with_attributes (-1, "Dirección", cell, "text", 8);
	col = view.get_column(8);
	col.set_resizable(true);
	col.set_visible(address_check);
	col.set_sort_column_id(8);
	view.insert_column_with_attributes (-1, "Localidad/Barrio", cell, "text", 9);
	col = view.get_column(9);
	col.set_resizable(true);
	col.set_visible(zone_check);
	col.set_sort_column_id(9);
	view.insert_column_with_attributes (-1, "Observaciones", cell, "text", 10);	
	col = view.get_column(10);
	col.set_resizable(true);
	col.set_visible(obs_check);
	col.set_sort_column_id(10);	
	view.insert_column_with_attributes (-1, "Ocupación", cell, "text", 11);
	col = view.get_column(11);
	col.set_resizable(true);
	col.set_visible(ocu_check);
	col.set_sort_column_id(11);
	view.insert_column_with_attributes (-1, "Sección", cell, "text", 12);
	col = view.get_column(12);
	col.set_resizable(true);
	col.set_visible(sec_check);
	col.set_sort_column_id(12);
	view.insert_column_with_attributes (-1, "Centro de trabajo", cell, "text", 13);
	col = view.get_column(13);
	col.set_resizable(true);
	col.set_visible(center_check);
	col.set_sort_column_id(13);
	view.insert_column_with_attributes (-1, "Dirección centro", cell, "text", 14);
	col = view.get_column(14);
	col.set_resizable(true);
	col.set_visible(address_c_check);
	col.set_sort_column_id(14);
	view.insert_column_with_attributes (-1, "Observaciones centro", cell, "text", 15);	
	col = view.get_column(15);
	col.set_resizable(true);
	col.set_visible(obs_c_check);	
	col.set_sort_column_id(15);
	view.insert_column_with_attributes (-1, "Fecha afiliación", cell, "text", 16);
	col = view.get_column(16);
	col.set_resizable(true);
	col.set_visible(date_check);	
	col.set_sort_column_id(16);
	view.insert_column_with_attributes (-1, "Mes cotizado", cell, "text", 17);	
	col = view.get_column(17);
	col.set_resizable(true);
	col.set_visible(cot_check);
	col.set_sort_column_id(17);	
	view.insert_column_with_attributes (-1, "Fecha baja", cell, "text", 18);	
	col = view.get_column(18);
	col.set_resizable(true);
	col.set_visible(out_check);
	col.set_sort_column_id(18);	
		
	Gtk.ScrolledWindow scroll = new Gtk.ScrolledWindow(null,null);
	scroll.add(view);
    
	// Se empaqueta como true para que se redimensione junto con la ventana
	vbox.pack_start(scroll, true, true, 0);
	
	Gtk.Label rows_label = new Gtk.Label(null);
	vbox.pack_start(rows_label, false, false, 0);
    
    // Se crea una barra de acciones y se le añaden botones
	Gtk.ActionBar actbar = new Gtk.ActionBar();
	Gtk.Button add_member = new Gtk.Button();
	Gtk.Image add_member_image = new Gtk.Image.from_icon_name("contact-new-symbolic", Gtk.IconSize.MENU);
	add_member.add(add_member_image);
	add_member.set_tooltip_text("Afiliar");
	
	Gtk.Button export = new Gtk.Button();
	Gtk.Image export_image = new Gtk.Image.from_icon_name("x-office-spreadsheet-symbolic", Gtk.IconSize.MENU);
	export.add(export_image);
	export.set_tooltip_text("Exportar");
	
	// Botón de menú (dentro de la barra de acción) y sus opciones
	Gtk.MenuButton actbutton = new Gtk.MenuButton();
	Gtk.Popover pop_menu = new Gtk.Popover(actbutton);
	actbutton.set_popover(pop_menu);
	Gtk.Image actimage = new Gtk.Image.from_icon_name("system-run-symbolic", Gtk.IconSize.MENU);
	actbutton.add(actimage);
	actbutton.set_tooltip_text("Opciones");
	
    actbar.pack_start(actbutton);
    actbar.pack_start(add_member);
    actbar.pack_start(export);
	
	vbox.pack_end(actbar, false, false, 0);
	
	// Se llama a las distintas funciones
	crea_lista_af(list_af, combo_af, rows_label);
	crea_menu_mostrar(pop,view, num_check, cc_check, name_check, surname1_check, surname2_check, birth_check, phone_check, email_check, address_check, zone_check, obs_check, ocu_check, sec_check, center_check, address_c_check, obs_c_check, date_check, cot_check, out_check);	
	crea_menu_filters(pop_fil, list_af, combo_af, rows_label);
	crea_menu_afiliacion(pop_menu, window, list_af, combo_af, rows_label);
	
	// Se conectan las señales
	add_member.clicked.connect ( () =>
	{
		var result = afiliar(window);
		if(result == true)
		{
			crea_lista_af(list_af, combo_af, rows_label);
		}
	});
	export.clicked.connect (() =>{exportar_af(window, combo_af);});
	combo_af.changed.connect (() =>
    {
        crea_lista_af(list_af, combo_af, rows_label);
        settings.set_int ("altas-bajas", combo_af.get_active());
    });
	showbutton.clicked.connect (() =>{pop.show_all();});
	filters.clicked.connect (() =>{pop_fil.show_all();});	
	actbutton.clicked.connect (() =>{pop_menu.show_all();});
    view.button_release_event.connect((event) =>
	{
        // Si se pulsa el botón derecho del ratón se crea el menú
        if (event.button == 3)
        {
            Gtk.TreePath treepath;
            if (view.get_path_at_pos ((int) event.x, (int) event.y, out treepath, null, null, null))
            {
					view.grab_focus();
					if (!selection.path_is_selected(treepath)) 
                    {
						selection.unselect_all();
						selection.select_path (treepath);
                    }
                    crea_menu_af(window, selection, list_af, combo_af, aux_button, rows_label); 
                    return true;
            }
        }
        else if (event.button == 1)
        {
            Gtk.TreePath treepath;
            if (!view.get_path_at_pos ((int) event.x, (int) event.y, out treepath, null, null, null))
            {
                selection.unselect_all();
            }
        }
        return false;
    });	     
	search.search_changed.connect (() =>
	{
		search_text = search.get_text();
		crea_lista_af(list_af, combo_af, rows_label);
	});	
    
    aux_button2.clicked.connect (() =>
    {
        if(first_flag_af == true)
        {
            first_flag_af = false;
            settings = new Settings ("cnt.tesoreria");
            var botones = settings.get_boolean ("botones");
            if(botones == true)
            {
                actbutton.set_visible(false);
            }
            else
            {
                add_member.set_visible(false);
                export.set_visible(false);
            }
        }
        else
        {
            var aux = actbutton.get_visible();
            actbutton.set_visible(!aux);
            aux = add_member.get_visible();
            add_member.set_visible(!aux);
            aux = export.get_visible();
            export.set_visible(!aux);
        }
    });
	
	// Se añade la caja al stack y se pone título al switcher
	stack.add_titled(vbox, "af", "Afiliación");	
}

void artilugios_ca_prev (Gtk.Stack stack, Gtk.ApplicationWindow window, Gtk.Button aux_button, Gtk.Button aux_button2, Gtk.SearchEntry search)
{ 
	Gtk.Box vbox = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
	Gtk.Box vbox1 = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
	Gtk.Box hbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
	
	Gtk.Label label = new Gtk.Label(null);
	label.set_markup("<big>Aún no existe ninguna caja. Crea la primera.</big>");
	vbox1.add(label);
	
	// Se añada una imagen para el caso de no existir cajas
	Gtk.Image image = new Gtk.Image.from_icon_name("folder-symbolic", Gtk.IconSize.DIALOG);
	image.set_pixel_size(256);
	image.set_opacity(0.4);
	vbox1.add(image);
	
	Gtk.Button add_cash = new Gtk.Button.with_label("Crear caja");
	hbox.pack_start(add_cash, false, false, 10);
	hbox.halign = Gtk.Align.CENTER;
	vbox1.add(hbox);
	
	vbox.set_center_widget(vbox1);
	
	// Se conecta la señal del botón
	add_cash.clicked.connect ( () =>
	{
		var result = crear_caja(window);
		if(result == true)
		{
			var child = stack.get_child_by_name("ca_prev");
			child.destroy();
			artilugios_ca(stack, window, aux_button, aux_button2, search);
			window.show_all();	
			child = stack.get_child_by_name("ca");
            first_flag_af = true;
            aux_button2.clicked();
			stack.visible_child = child;
			cajas++;
		}
	});

	// Se añade la caja al stack y se pone título al switcher	
	stack.add_titled(vbox, "ca_prev", "Cajas");	
}

void artilugios_ca (Gtk.Stack stack, Gtk.ApplicationWindow window, Gtk.Button aux_button, Gtk.Button aux_button2, Gtk.SearchEntry search)
{
	Gtk.Box vbox = new Gtk.Box(Gtk.Orientation.VERTICAL, 10);
	Gtk.Box hbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
		
	Gtk.ListStore list_ca = new Gtk.ListStore(1, typeof(string));
	Gtk.TreeIter iter_ca;	
	list_ca.append(out iter_ca); 
	
	// Se crea la lista de cajas existentes en la base de datos
	db.exec("SELECT name FROM sqlite_master WHERE name != 'altas' AND name != 'bajas' AND name != 'cotizaciones'", (n_columns, values, column_names) => 
    {
		list_ca.set(iter_ca, 0, values[0]);
		list_ca.append(out iter_ca);
       return 0;
    }, null);
	list_ca.remove(ref iter_ca);
	
	Gtk.ComboBox combo_ca = new Gtk.ComboBox.with_model(list_ca);	
	Gtk.CellRendererText cell = new Gtk.CellRendererText ();
	combo_ca.pack_start(cell,false);
	combo_ca.set_attributes (cell, "text", 0);
	
	// Se toma del schema el número de caja y se selecciona
	var settings = new Settings ("cnt.tesoreria");
	var sett_caja = settings.get_int ("caja");
	combo_ca.set_active(sett_caja);
	combo_ca.set_tooltip_text("Seleccionar caja");
	hbox.pack_start(combo_ca, true, false, 0);
	
	Gtk.Box hbox_since = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
	
	Gtk.Label label_since = new Gtk.Label("Desde:");
	hbox_since.add(label_since);
		
	Gtk.Entry date_since = new Gtk.Entry();
	date_since.set_editable(true);
	date_since.set_property("width-chars", 12);
	date_since.set_placeholder_text("dd/mm/aaaa");
	date_since.max_length = 10;
	date_since.set_icon_from_icon_name(0, "x-office-calendar-symbolic");
	texto_desde_hasta(date_since, true);
	// Al pulsar en el icono se llama a calendario
	date_since.icon_press.connect (() =>
	{
		date_since.grab_focus();
		calendario(date_since);
	});
	hbox_since.add(date_since);

	hbox.pack_start(hbox_since, true, false, 0);
	
	Gtk.Box hbox_to = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
	
	Gtk.Label label_to = new Gtk.Label("Hasta:");
	hbox_to.add(label_to);
			
	Gtk.Entry date_to = new Gtk.Entry();
	date_to.set_editable(true);
	date_to.set_property("width-chars", 12);
	date_to.set_placeholder_text("dd/mm/aaaa");
	date_to.max_length = 10;
	date_to.set_icon_from_icon_name(0, "x-office-calendar-symbolic");			
	texto_desde_hasta(date_to, false);
	// Al pulsar en el icono se llama a calendario
	date_to.icon_press.connect (() =>
	{
		date_to.grab_focus();
		calendario(date_to);
	});
	hbox_to.add(date_to);
	
	hbox.pack_start(hbox_to, true, false, 0);
	
	Gtk.ListStore list_fil = new Gtk.ListStore(1, typeof(string));
	Gtk.TreeIter iter_fil;
	list_fil.append(out iter_fil);
	list_fil.set(iter_fil, 0, "Todos los movimientos");
	list_fil.append(out iter_fil);
	list_fil.set(iter_fil, 0, "Entradas");
	list_fil.append(out iter_fil);
	list_fil.set(iter_fil, 0, "Salidas");		
	Gtk.ComboBox combo_fil = new Gtk.ComboBox.with_model(list_fil);
	combo_fil.pack_start(cell,false);
	combo_fil.set_attributes (cell, "text", 0);
	combo_fil.set_active(0);
	combo_fil.set_tooltip_text("Seleccionar filtro");
	hbox.pack_start(combo_fil, true, false, 0);

	vbox.pack_start(hbox, false, false, 0);
	
	Gtk.ListStore list_mov = new Gtk.ListStore (6, typeof(string), typeof(string), typeof(string),typeof(string), typeof(string), typeof(string));
	Gtk.TreeView view = new Gtk.TreeView.with_model(list_mov);
	view.set_enable_search(false);
	var selection = view.get_selection();
	selection.set_mode(Gtk.SelectionMode.NONE);
		
	view.insert_column_with_attributes (-1, "Número", cell, "text", 0);
	var col = view.get_column(0);
	col.set_resizable(true);
	view.insert_column_with_attributes (-1, "Fecha", cell, "text", 1);
	col = view.get_column(1);
	col.set_resizable(true);
	view.insert_column_with_attributes (-1, "Factura", cell, "text", 2);
	col = view.get_column(2);
	col.set_resizable(true);
	view.insert_column_with_attributes (-1, "Concepto", cell, "text", 3);
	col = view.get_column(3);
	col.set_resizable(true);	
	col.set_expand(true);
	view.insert_column_with_attributes (-1, "Importe", cell, "text", 4);
	col = view.get_column(4);
	col.set_resizable(true);	
	view.insert_column_with_attributes (-1, "Saldo", cell, "text", 5);	
	col = view.get_column(5);
	col.set_resizable(true);	
	
	Gtk.ScrolledWindow scroll = new Gtk.ScrolledWindow(null,null);
	scroll.add(view);

	// Se empaqueta como true para que se redimensione junto con la ventana
	vbox.pack_start(scroll, true, true, 0);
	
	Gtk.Label rows_label = new Gtk.Label(null);
	vbox.pack_start(rows_label, false, false, 0);
	
	// Se crea una barra de acciones y se le añaden botones
	Gtk.ActionBar actbar = new Gtk.ActionBar();
	Gtk.Button trans = new Gtk.Button.with_label("Transferencia");
	trans.set_tooltip_text("Añadir transferencia");
	Gtk.Button mov = new Gtk.Button.with_label("Movimiento");
	mov.set_tooltip_text("Añadir movimiento");
	
	Gtk.Button add_cash = new Gtk.Button();
	Gtk.Image add_cash_image = new Gtk.Image.from_icon_name("list-add-symbolic", Gtk.IconSize.MENU);
	add_cash.add(add_cash_image);
	add_cash.set_tooltip_text("Crear caja");
	
	Gtk.Button remove_cash = new Gtk.Button();
	Gtk.Image remove_cash_image = new Gtk.Image.from_icon_name("edit-delete-symbolic", Gtk.IconSize.MENU);
	remove_cash.add(remove_cash_image);
	remove_cash.set_tooltip_text("Eliminar caja");
	
	Gtk.Button check_cash = new Gtk.Button();
	Gtk.Image check_cash_image = new Gtk.Image.from_icon_name("accessories-calculator-symbolic", Gtk.IconSize.MENU);
	check_cash.add(check_cash_image);
	check_cash.set_tooltip_text("Cuadrar caja");
	
	Gtk.Button cash = new Gtk.Button();
	Gtk.Image cash_image = new Gtk.Image.from_icon_name("utilities-system-monitor-symbolic", Gtk.IconSize.MENU);
	cash.add(cash_image);
	cash.set_tooltip_text("Saldos");			
	
	Gtk.Button export = new Gtk.Button();
	Gtk.Image export_image = new Gtk.Image.from_icon_name("x-office-spreadsheet-symbolic", Gtk.IconSize.MENU);
	export.add(export_image);
	export.set_tooltip_text("Exportar");
	
	// Botón de menú (dentro de la barra de acción) y sus opciones
	Gtk.MenuButton actbutton = new Gtk.MenuButton();
	Gtk.Popover pop = new Gtk.Popover(actbutton);
	actbutton.set_popover(pop);
	Gtk.Image actimage = new Gtk.Image.from_icon_name("system-run-symbolic", Gtk.IconSize.MENU);
	actbutton.add(actimage);
	actbutton.set_tooltip_text("Opciones");
	
	actbar.pack_start(actbutton);
	actbar.pack_start(add_cash);
	actbar.pack_start(remove_cash);
	actbar.pack_start(check_cash);
	actbar.pack_start(cash);
	actbar.pack_start(export);
	actbar.pack_end(mov);
	actbar.pack_end(trans);
	
	vbox.pack_end(actbar, false, false, 0);
	
	// Se llama a las distintas funciones
	crea_lista_ca(list_mov, combo_ca, date_since, date_to, combo_fil, rows_label);
	crea_menu_cajas(pop, window, list_mov, combo_ca, date_since, date_to, combo_fil, rows_label);
	
	// Se conectan las señales
	trans.clicked.connect ( () =>
	{
		var result = transferencia(window, combo_ca);
		if(result == true)
		{
			crea_lista_ca(list_mov, combo_ca, date_since, date_to, combo_fil, rows_label);
		}
	});
	mov.clicked.connect ( () =>
	{
		var result = movimiento(window, combo_ca);
		if(result == true)
		{
			crea_lista_ca(list_mov, combo_ca, date_since, date_to, combo_fil, rows_label);
		}
	});	
	combo_ca.changed.connect (() =>
	{
		crea_lista_ca(list_mov, combo_ca, date_since, date_to, combo_fil, rows_label);
		settings.set_int ("caja", combo_ca.get_active());
	});
	date_since.changed.connect (() =>{crea_lista_ca(list_mov, combo_ca, date_since, date_to, combo_fil, rows_label);});
	date_to.changed.connect (() =>{crea_lista_ca(list_mov, combo_ca, date_since, date_to, combo_fil, rows_label);});
	combo_fil.changed.connect (() =>{crea_lista_ca(list_mov, combo_ca, date_since, date_to, combo_fil, rows_label);});
	actbutton.clicked.connect (() =>{pop.show_all();});
	aux_button.clicked.connect (() =>{crea_lista_ca(list_mov, combo_ca, date_since, date_to, combo_fil, rows_label);});
	search.search_changed.connect (() =>
	{
		crea_lista_ca(list_mov, combo_ca, date_since, date_to, combo_fil, rows_label);
	});
	add_cash.clicked.connect ( () =>
	{
		var result = crear_caja(window);
		if(result == true)
		{
			actualiza_combo_ca (window, combo_ca);
			cajas++;
		}
	});
	check_cash.clicked.connect ( () =>
	{
		var result = cuadrar(window, combo_ca);
		if(result == true)
		{
			crea_lista_ca(list_mov, combo_ca, date_since, date_to, combo_fil, rows_label);
		}
	});
	remove_cash.clicked.connect ( () =>{eliminar_caja(window, combo_ca);});
	export.clicked.connect ( () =>{exportar_ca(window, list_mov, combo_ca, date_since, date_to, combo_fil);});
	cash.clicked.connect ( () =>{saldos(window);});
   
    aux_button2.clicked.connect (() =>
    {
        if(first_flag_ca == true)
        {
            first_flag_ca = false;
            settings = new Settings ("cnt.tesoreria");
            var botones = settings.get_boolean ("botones");
            if(botones == true)
            {
                actbutton.set_visible(false);
            }
            else
            {
                add_cash.set_visible(false);
                remove_cash.set_visible(false);
                check_cash.set_visible(false);
                cash.set_visible(false);
                export.set_visible(false);
            }
        }
        else
        {
            var aux = actbutton.get_visible();
            actbutton.set_visible(!aux);
            aux = add_cash.get_visible();
            add_cash.set_visible(!aux);
            aux = remove_cash.get_visible();
            remove_cash.set_visible(!aux);
            aux = check_cash.get_visible();
            check_cash.set_visible(!aux);
            aux = cash.get_visible();
            cash.set_visible(!aux);
            aux = export.get_visible();
            export.set_visible(!aux);
        }
    });

	// Se añade la caja al stack y se pone título al switcher	
	stack.add_titled(vbox, "ca", "Cajas");	
}

void actualiza_combo_ca (Gtk.ApplicationWindow window, Gtk.ComboBox combo)
{	
	Gtk.ListStore list_ca = new Gtk.ListStore(1, typeof(string));
	Gtk.TreeIter iter_ca;	
	 
	list_ca.append(out iter_ca); 
	db.exec("SELECT name FROM sqlite_master WHERE name != 'altas' AND name != 'bajas' AND name != 'cotizaciones'", (n_columns, values, column_names) => 
    {
		list_ca.set(iter_ca, 0, values[0]);
		list_ca.append(out iter_ca);
        return 0;
    }, null);
	list_ca.remove(ref iter_ca);
	combo.set_model(list_ca);
	combo.set_active(0);	 
}

void crea_lista_ca(Gtk.ListStore list_mov, Gtk.ComboBox combo_ca, Gtk.Entry since, Gtk.Entry to, Gtk.ComboBox combo_fil, Gtk.Label rows_label)
{
	int rows = 0;
	list_mov.clear();
	
	var texto_since = formatea_texto(since, 0);
	var texto_to = formatea_texto(to, 1);
	
	if(texto_since != "" && texto_to != "")
	{
		double money = 0.0;
		Gtk.TreeIter iter_pre;
		combo_ca.get_active_iter(out iter_pre);
		var model = combo_ca.get_model();
		Value name;
		model.get_value(iter_pre, 0, out name);
		var name_s = (string) name;
		
		if(search_text == "")
		{
			if(combo_fil.get_active() == 0)
			{
				db.exec(@"SELECT SUM(euros) FROM $name_s WHERE fecha < '$texto_since'", (n_columns, values, column_names) => 
				{
					if(values[0] != null)
					{	
						money = double.parse(values[0]);
					}
					return 0;
					}, null);
				db.exec(@"SELECT * FROM $name_s WHERE fecha >= '$texto_since' AND fecha <= '$texto_to' ORDER BY fecha", (n_columns, values, column_names) => 
				{
					money = aux_lista_ca(list_mov, values, money, true);	
					rows++;
					return 0;
				}, null);
			}
			else if(combo_fil.get_active() == 1)
			{				
				db.exec(@"SELECT * FROM $name_s WHERE fecha >= '$texto_since' AND fecha <= '$texto_to' AND euros > '0' ORDER BY fecha", (n_columns, values, column_names) => 
				{
					aux_lista_ca(list_mov, values, money, false);	
					rows++;			
					return 0;
				}, null);				
			}	
			else
			{
				db.exec(@"SELECT * FROM $name_s WHERE fecha >= '$texto_since' AND fecha <= '$texto_to' AND euros < '0' ORDER BY fecha", (n_columns, values, column_names) => 
				{	
					aux_lista_ca(list_mov, values, money, false);	
					rows++;
					return 0;
				}, null);					
			}				
		}
		else
		{
			if(combo_fil.get_active() == 0)
			{
				db.exec(@"SELECT * FROM $name_s WHERE fecha >= '$texto_since' AND fecha <= '$texto_to' AND (num LIKE '%$search_text%' OR fecha LIKE '%$search_text%' OR factura LIKE '%$search_text%' OR concepto LIKE '%$search_text%' OR euros LIKE '%$search_text%') ORDER BY fecha", (n_columns, values, column_names) => 
				{	
					aux_lista_ca(list_mov, values, money, false);	
					rows++;
					return 0;
				}, null);			
			}
			else if(combo_fil.get_active() == 1)
			{
				db.exec(@"SELECT * FROM $name_s WHERE fecha >= '$texto_since' AND fecha <= '$texto_to' AND euros > '0' AND (num LIKE '%$search_text%' OR fecha LIKE '%$search_text%' OR factura LIKE '%$search_text%' OR concepto LIKE '%$search_text%' OR euros LIKE '%$search_text%') ORDER BY fecha", (n_columns, values, column_names) =>
				{	
					aux_lista_ca(list_mov, values, money, false);	
					rows++;
					return 0;
				}, null);					
			}	
			else
			{
				db.exec(@"SELECT * FROM $name_s WHERE fecha >= '$texto_since' AND fecha <= '$texto_to' AND euros < '0' AND (num LIKE '%$search_text%' OR fecha LIKE '%$search_text%' OR factura LIKE '%$search_text%' OR concepto LIKE '%$search_text%' OR euros LIKE '%$search_text%') ORDER BY fecha", (n_columns, values, column_names) =>
				{
					aux_lista_ca(list_mov, values, money, false);		
					rows++;
					return 0;
				}, null);					
			}
		}
	}
	if(rows == 1)
	{
		rows_label.set_text("1 fila");
	}
	else
	{
		rows_label.set_text(@"$rows filas");
	}
}

void crea_menu_afiliacion(Gtk.Popover pop, Gtk.ApplicationWindow window, Gtk.ListStore list_af, Gtk.ComboBox combo_af, Gtk.Label rows_label)
{				
	// Se crea una caja y se añade al menú
	Gtk.Box boxpop = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
	boxpop.set_property("margin", 10);
	pop.add(boxpop);
				
	// Se añaden elementos a la caja del menú
	Gtk.ModelButton add_member = new Gtk.ModelButton();
	add_member.set_property("text", "Afiliar");
	boxpop.pack_start(add_member);
				
	Gtk.ModelButton export = new Gtk.ModelButton();
	export.set_property("text", "Exportar");
	boxpop.pack_start(export);
				
	// Se conectan las señales con las funciones
	add_member.clicked.connect ( () =>
	{
		var result = afiliar(window);
		if(result == true)
		{
			crea_lista_af(list_af, combo_af, rows_label);
		}
	});
	export.clicked.connect (() =>{exportar_af(window, combo_af);});
}

void crea_menu_cajas(Gtk.Popover pop, Gtk.ApplicationWindow window, Gtk.ListStore list_mov, Gtk.ComboBox combo_ca, Gtk.Entry since, Gtk.Entry to, Gtk.ComboBox combo_fil, Gtk.Label rows_label)
{				
	// Se crea una caja y se añade al menú
	Gtk.Box boxpop = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
	boxpop.set_property("margin", 10);
	pop.add(boxpop);
				
	// Se añaden elementos a la caja del menú
	Gtk.ModelButton add_cash = new Gtk.ModelButton();
	add_cash.set_property("text", "Crear caja");
	boxpop.pack_start(add_cash);
				
	Gtk.ModelButton remove_cash = new Gtk.ModelButton();
	remove_cash.set_property("text", "Eliminar caja");
	boxpop.pack_start(remove_cash);
	
	Gtk.ModelButton check_cash = new Gtk.ModelButton();
	check_cash.set_property("text", "Cuadrar caja");
	boxpop.pack_start(check_cash);
				
	Gtk.ModelButton export = new Gtk.ModelButton();
	export.set_property("text", "Exportar");
	boxpop.pack_start(export);
	
	Gtk.ModelButton cash = new Gtk.ModelButton();
	cash.set_property("text", "Saldos");
	boxpop.pack_start(cash);
				
	// Se conectan las señales con las funciones
	add_cash.clicked.connect ( () =>
	{
		var result = crear_caja(window);
		if(result == true)
		{
			actualiza_combo_ca (window, combo_ca);
			cajas++;
		}
	});
	check_cash.clicked.connect ( () =>
	{
		var result = cuadrar(window, combo_ca);
		if(result == true)
		{
			crea_lista_ca(list_mov, combo_ca, since, to, combo_fil, rows_label);
		}
	});
	remove_cash.clicked.connect ( () =>{eliminar_caja(window, combo_ca);});
	export.clicked.connect ( () =>{exportar_ca(window, list_mov, combo_ca, since, to, combo_fil);});
	cash.clicked.connect ( () =>{saldos(window);});
}

void crea_lista_af(Gtk.ListStore list, Gtk.ComboBox combo_af, Gtk.Label rows_label)
{
	int rows = 0;
	string table;
	list.clear();
	Gtk.TreeIter iter;
	list.prepend(out iter);
	
	if (combo_af.get_active() == 0)
	{
		table = "altas";
	}
	else
	{
		table = "bajas";
	}
	
	if(search_text == "")
	{
		if(filters_s == "all")	
		{
			db.exec(@"SELECT * FROM $table ORDER BY apellido1 DESC, apellido2 DESC, nombre DESC", (n_columns, values, column_names) => 
			{
				aux_lista_af(list, values, table);
				rows++;
				return 0;
			}, null);	
		}
		else if(filters_s == "exempt")
		{	
			db.exec(@"SELECT * FROM $table WHERE cotizado = 'exento/a' ORDER BY apellido1 DESC, apellido2 DESC, nombre DESC", (n_columns, values, column_names) => 
			{
				aux_lista_af(list, values, table);
				rows++;
				return 0;
			}, null);		
		}
		else if(filters_s == "debtors")
		{
			var now = new DateTime.now_local();
			var month_now = int.parse(now.format("%m"));
			var year_i = int.parse(now.format("%Y"));
			var month_i = month_now - months_fil;
			if(month_i < 1)
			{
				year_i--;
				month_i = month_i + 12;
			}
			string cot_req;
			if(month_i < 10)
			{
				cot_req = @"$year_i-0$month_i";
			}
			else
			{
				cot_req = @"$year_i-$month_i";
			}
			db.exec(@"SELECT * FROM $table WHERE cotizado <= '$cot_req' ORDER BY apellido1 DESC, apellido2 DESC, nombre DESC", (n_columns, values, column_names) => 
			{
				aux_lista_af(list, values, table);
				rows++;
				return 0;
			}, null);			
		}	
	}
	else
	{
		string search_date_out = "";
		if(table == "bajas")
		{
			search_date_out = "OR fechabaja LIKE '%$search_text%'";
		}
		if(filters_s == "all")
		{	
			db.exec(@"SELECT * FROM $table WHERE cc LIKE '%$search_text%' OR nombre LIKE '%$search_text%' OR apellido1 LIKE '%$search_text%' OR apellido2 LIKE '%$search_text%' OR fnacimiento LIKE '%$search_text%' OR telefono LIKE '%$search_text%' OR email LIKE '%$search_text%' OR dir LIKE '%$search_text%' OR zona LIKE '%$search_text%' OR obs LIKE '%$search_text%' OR ocu LIKE '%$search_text%' OR sec LIKE '%$search_text%' OR centro LIKE '%$search_text%' OR dirc LIKE '%$search_text%' OR obsc LIKE '%$search_text%' OR afiliado LIKE '%$search_text%' $search_date_out ORDER BY apellido1 DESC, apellido2 DESC, nombre DESC", (n_columns, values, column_names) => 
			{
				aux_lista_af(list, values, table);
				rows++;
				return 0;
			}, null);		
		}
		else if(filters_s == "exempt")
		{
			db.exec(@"SELECT * FROM $table WHERE cotizado = 'exento/a' AND (cc LIKE '%$search_text%' OR nombre LIKE '%$search_text%' OR apellido1 LIKE '%$search_text%' OR apellido2 LIKE '%$search_text%' OR fnacimiento LIKE '%$search_text%' OR telefono LIKE '%$search_text%' OR email LIKE '%$search_text%' OR dir LIKE '%$search_text%' OR zona LIKE '%$search_text%' OR obs LIKE '%$search_text%' OR ocu LIKE '%$search_text%' OR sec LIKE '%$search_text%' OR centro LIKE '%$search_text%' OR dirc LIKE '%$search_text%' OR obsc LIKE '%$search_text%' OR afiliado LIKE '%$search_text%' $search_date_out) ORDER BY apellido1 DESC, apellido2 DESC, nombre DESC", (n_columns, values, column_names) =>
			{
				aux_lista_af(list, values, table);
				rows++;
				return 0;
			}, null);			
		}
		else if(filters_s == "debtors")
		{
			var now = new DateTime.now_local();
			var month_now = int.parse(now.format("%m"));
			var year_i = int.parse(now.format("%Y"));
			var month_i = month_now - months_fil;
			if(month_i < 1)
			{
				year_i--;
				month_i = month_i + 12;
			}
			string cot_req;
			if(month_i < 10)
			{
				cot_req = @"$year_i-0$month_i";
			}
			else
			{
				cot_req = @"$year_i-$month_i";
			}
			db.exec(@"SELECT * FROM $table WHERE cotizado <= '$cot_req' AND (cc LIKE '%$search_text%' OR nombre LIKE '%$search_text%' OR apellido1 LIKE '%$search_text%' OR apellido2 LIKE '%$search_text%' OR fnacimiento LIKE '%$search_text%' OR telefono LIKE '%$search_text%' OR email LIKE '%$search_text%' OR dir LIKE '%$search_text%' OR zona LIKE '%$search_text%' OR obs LIKE '%$search_text%' OR ocu LIKE '%$search_text%' OR sec LIKE '%$search_text%' OR centro LIKE '%$search_text%' OR dirc LIKE '%$search_text%' OR obsc LIKE '%$search_text%' OR afiliado LIKE '%$search_text%' $search_date_out) ORDER BY apellido1 DESC, apellido2 DESC, nombre DESC", (n_columns, values, column_names) =>
			{
				aux_lista_af(list, values, table);
				rows++;
				return 0;
			}, null);				
		}
	}
	list.remove(ref iter);
	if(rows == 1)
	{
		rows_label.set_text("1 fila");
	}
	else
	{
		rows_label.set_text(@"$rows filas");
	}
}	

void crea_menu_mostrar(Gtk.Popover pop, Gtk.TreeView view, bool num_check, bool cc_check, bool name_check, bool surname1_check, bool surname2_check, bool birth_check, bool phone_check, bool email_check, bool address_check, bool zone_check, bool obs_check, bool ocu_check, bool sec_check, bool center_check, bool address_c_check, bool obs_c_check, bool date_check, bool cot_check, bool out_check)
{
	// Se crea una caja y se añade al menú
	Gtk.Box boxpop = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
	boxpop.set_property("margin", 10);
	pop.add(boxpop);
				
	// Se añaden elementos a la caja del menú
	Gtk.CheckButton num = new Gtk.CheckButton.with_label("Número");
	num.set_active(num_check);
	boxpop.pack_start(num);
	
	Gtk.CheckButton cc = new Gtk.CheckButton.with_label("CC");
	cc.set_active(cc_check);
	boxpop.pack_start(cc);
	
	Gtk.CheckButton surname1 = new Gtk.CheckButton.with_label("Primer Apellido");
	surname1.set_active(surname1_check);
	boxpop.pack_start(surname1);	
	
	Gtk.CheckButton surname2 = new Gtk.CheckButton.with_label("Segundo Apellido");
	surname2.set_active(surname2_check);
	boxpop.pack_start(surname2);
	
	Gtk.CheckButton name = new Gtk.CheckButton.with_label("Nombre");
	name.set_active(name_check);
	boxpop.pack_start(name);
	
	Gtk.CheckButton birth = new Gtk.CheckButton.with_label("Fecha de nacimiento");
	birth.set_active(birth_check);
	boxpop.pack_start(birth);
	
	Gtk.CheckButton phone = new Gtk.CheckButton.with_label("Teléfono");
	phone.set_active(phone_check);
	boxpop.pack_start(phone);	
	
	Gtk.CheckButton mail = new Gtk.CheckButton.with_label("e-mail");
	mail.set_active(email_check);
	boxpop.pack_start(mail);	
	
	Gtk.CheckButton address = new Gtk.CheckButton.with_label("Dirección");
	address.set_active(address_check);
	boxpop.pack_start(address);
	
	Gtk.CheckButton zone = new Gtk.CheckButton.with_label("Localidad/Barrio");
	zone.set_active(zone_check);
	boxpop.pack_start(zone);
	
	Gtk.CheckButton obs = new Gtk.CheckButton.with_label("Observaciones");
	obs.set_active(obs_check);
	boxpop.pack_start(obs);	
	
	Gtk.CheckButton ocu = new Gtk.CheckButton.with_label("Ocupación");
	ocu.set_active(ocu_check);
	boxpop.pack_start(ocu);
	
	Gtk.CheckButton sec = new Gtk.CheckButton.with_label("Sección");
	sec.set_active(sec_check);
	boxpop.pack_start(sec);
	
	Gtk.CheckButton center = new Gtk.CheckButton.with_label("Centro de trabajo");
	center.set_active(center_check);
	boxpop.pack_start(center);
	
	Gtk.CheckButton address_c = new Gtk.CheckButton.with_label("Dirección centro");
	address_c.set_active(address_c_check);
	boxpop.pack_start(address_c);	
	
	Gtk.CheckButton obs_c = new Gtk.CheckButton.with_label("Observaciones centro");
	obs_c.set_active(obs_c_check);
	boxpop.pack_start(obs_c);
	
	Gtk.CheckButton date = new Gtk.CheckButton.with_label("Fecha afiliación");
	date.set_active(date_check);
	boxpop.pack_start(date);
	
	Gtk.CheckButton cot = new Gtk.CheckButton.with_label("Mes cotizado");
	cot.set_active(cot_check);
	boxpop.pack_start(cot);
	
	Gtk.CheckButton date_out = new Gtk.CheckButton.with_label("Fecha baja");
	date_out.set_active(out_check);
	boxpop.pack_start(date_out);					
	
	// Se conectan las señales con las funciones
	var settings = new Settings ("cnt.tesoreria.mostrar");
	
	num.clicked.connect ( () =>
	{
		var show = num.get_active();
		var col = view.get_column(0);
		col.set_visible(show);
		settings.set_boolean ("numero", show);
	});
	cc.clicked.connect ( () =>
	{
		var show = cc.get_active();
		var col = view.get_column(1);
		col.set_visible(show);	
		settings.set_boolean ("cc", show);
	});
	surname1.clicked.connect ( () =>
	{
		var show = surname1.get_active();
		var col = view.get_column(2);
		col.set_visible(show);
		settings.set_boolean ("apellido1", show);		
	});
	surname2.clicked.connect ( () =>
	{
		var show = surname2.get_active();
		var col = view.get_column(3);
		col.set_visible(show);
		settings.set_boolean ("apellido2", show);		
	});
	name.clicked.connect ( () =>
	{
		var show = name.get_active();
		var col = view.get_column(4);
		col.set_visible(show);	
		settings.set_boolean ("nombre", show);	
	});
	birth.clicked.connect ( () =>
	{
		var show = birth.get_active();
		var col = view.get_column(5);
		col.set_visible(show);	
		settings.set_boolean ("fnacimiento", show);		
	});
	phone.clicked.connect ( () =>
	{
		var show = phone.get_active();
		var col = view.get_column(6);
		col.set_visible(show);
		settings.set_boolean ("telefono", show);		
	});
	mail.clicked.connect ( () =>
	{
		var show = mail.get_active();
		var col = view.get_column(7);
		col.set_visible(show);	
		settings.set_boolean ("email", show);		
	});
	address.clicked.connect ( () =>
	{
		var show = address.get_active();
		var col = view.get_column(8);
		col.set_visible(show);	
		settings.set_boolean ("dir", show);	
	});
	zone.clicked.connect ( () =>
	{
		var show = zone.get_active();
		var col = view.get_column(9);
		col.set_visible(show);
		settings.set_boolean ("zona", show);		
	});
	obs.clicked.connect ( () =>
	{
		var show = obs.get_active();
		var col = view.get_column(10);
		col.set_visible(show);	
		settings.set_boolean ("obs", show);	
	});
	ocu.clicked.connect ( () =>
	{
		var show = ocu.get_active();
		var col = view.get_column(11);
		col.set_visible(show);
		settings.set_boolean ("ocu", show);			
	});
	sec.clicked.connect ( () =>
	{
		var show = sec.get_active();
		var col = view.get_column(12);
		col.set_visible(show);	
		settings.set_boolean ("sec", show);	
	});
	center.clicked.connect ( () =>
	{
		var show = center.get_active();
		var col = view.get_column(13);
		col.set_visible(show);	
		settings.set_boolean ("centro", show);	
	});
	address_c.clicked.connect ( () =>
	{
		var show = address_c.get_active();
		var col = view.get_column(14);
		col.set_visible(show);	
		settings.set_boolean ("dirc", show);	
	});
	obs_c.clicked.connect ( () =>
	{
		var show = obs_c.get_active();
		var col = view.get_column(15);
		col.set_visible(show);
		settings.set_boolean ("obsc", show);		
	});
	date.clicked.connect ( () =>
	{
		var show = date.get_active();
		var col = view.get_column(16);
		col.set_visible(show);
		settings.set_boolean ("afiliado", show);		
	});
	cot.clicked.connect ( () =>
	{
		var show = cot.get_active();
		var col = view.get_column(17);
		col.set_visible(show);
		settings.set_boolean ("cotizado", show);		
	});		
	date_out.clicked.connect ( () =>
	{
		var show = date_out.get_active();
		var col = view.get_column(18);
		col.set_visible(show);
		settings.set_boolean ("fechabaja", show);			
	});	
}

void crea_menu_filters(Gtk.Popover pop, Gtk.ListStore list, Gtk.ComboBox combo_af, Gtk.Label rows_label)
{
	// Se crea una caja y se añade al menú
	Gtk.Box boxpop = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
	boxpop.set_property("margin", 10);
	pop.add(boxpop);
				
	// Se añaden elementos a la caja del menú
	Gtk.RadioButton all = new Gtk.RadioButton.with_label(null,"Toda la afiliación");
	boxpop.pack_start(all);			
	
	Gtk.RadioButton exempt = new Gtk.RadioButton.with_label(all.get_group(),"Exentos/as");
	boxpop.pack_start(exempt);				
	
	Gtk.RadioButton debtors = new Gtk.RadioButton.with_label(all.get_group(),"Deudores/as");
	boxpop.pack_start(debtors);	
	
	Gtk.Box hbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
	Gtk.Label label = new Gtk.Label("Meses:");
	hbox.pack_start(label);
	Gtk.SpinButton months = new Gtk.SpinButton.with_range(1,6,1);
	months.set_value(6);
	months.set_sensitive(false);
	hbox.pack_start(months);
	boxpop.pack_start(hbox, false, false, 5);						
	
	// Se conectan las señales con las funciones
	all.toggled.connect ( () =>
	{
		months.set_sensitive(false);
		filters_s = "all";
		crea_lista_af(list, combo_af, rows_label);
	});
	exempt.toggled.connect ( () =>
	{
		months.set_sensitive(false);
		filters_s = "exempt";
		crea_lista_af(list, combo_af, rows_label);
	});
	debtors.toggled.connect ( () =>
	{
		months.set_sensitive(true);
		filters_s = "debtors";
		crea_lista_af(list, combo_af, rows_label);
	});		
	months.value_changed.connect ( () =>
	{
		months_fil = months.get_value_as_int();
		crea_lista_af(list, combo_af, rows_label);
	});	
}


void crea_menu_af(Gtk.ApplicationWindow window, Gtk.TreeSelection selection, Gtk.ListStore list, Gtk.ComboBox combo_af, Gtk.Button aux_button, Gtk.Label rows_label)
{
	Gtk.Menu menu = new Gtk.Menu();

	Gtk.TreeModel model;
	Gtk.TreeIter iter;
	Value cot;
	selection.get_selected(out model, out iter);
	model.get_value (iter, 17, out cot);
	var cot_s = (string) cot;
	
	// Se añaden elementos al menú									
	Gtk.MenuItem quotes = new Gtk.MenuItem.with_label("Cotizar");
	if(cot_s != "exento/a")
	{
		menu.attach(quotes, 0, 1, 0, 1);
	}
    
    Gtk.MenuItem edit = new Gtk.MenuItem.with_label("Editar");
    menu.attach(edit, 0, 1, 1, 2);
		
    Gtk.MenuItem edit_cc = new Gtk.MenuItem.with_label("Editar CC");
    menu.attach(edit_cc, 0, 1, 2, 3);
	
	// Solo en altas
	if (combo_af.get_active() == 0)
	{
		Gtk.MenuItem down = new Gtk.MenuItem.with_label("Causar baja");
		menu.attach(down, 0, 1, 3, 4);
	
		// Se conectan la señal con las funciones			
		down.activate.connect ( () =>
		{
			causar_baja(window, selection, list, rows_label);
		});	
	}
	// Solo en bajas
	else
	{
		// Se añaden elementos al menú		
		Gtk.MenuItem reincor = new Gtk.MenuItem.with_label("Reincorporar");
		menu.attach(reincor, 0, 1, 3, 4);
		
		Gtk.MenuItem delete_ = new Gtk.MenuItem.with_label("Eliminar");
		menu.attach(delete_, 0, 1, 4, 5);

		// Se conectan las señales con las funciones
		reincor.activate.connect ( () =>
		{
			reincorporar_baja(window, selection, list, rows_label);
		});	
		delete_.activate.connect ( () =>
		{
			eliminar_baja(window, selection, list, rows_label);
		});				
	}
    // Se conectan las señales con las funciones
	quotes.activate.connect ( () =>
	{
		var result = cotizar(window, selection, combo_af.get_active(), list);
		if(result == true)
		{
			aux_button.clicked();
		}
	});	
    edit.activate.connect ( () =>
	{
		editar(window, selection, combo_af.get_active(), list);
	});	
	edit_cc.activate.connect ( () =>
	{
		editar_cc(window, selection, combo_af.get_active(), list);
	});	    
	menu.show_all();
	menu.popup_at_pointer(null);	
}

double aux_lista_ca(Gtk.ListStore list_mov, string[] values, double money, bool saldo)
{
	Gtk.TreeIter iter_mov;
	list_mov.prepend(out iter_mov);
	char[] buffer = new char[20];
	
	string num = values[0];
	string fech_total = values[1];
	string year = fech_total[0:4];
	string month = fech_total[5:7];
	string day = fech_total[8:10];
	string fech = @"$day/$month/$year";			
					
	string fact = values[2];
	string conc = values[3];
	double imp = double.parse(values[4]);
	
	string money_s = "";
	if(saldo == true)
	{		
		money = money + imp;
		money_s = (money.format (buffer, "%.2f")).replace(".",",") + " € ";
	}
	string imp_s = (imp.format (buffer, "%.2f")).replace(".",",") + " €";
				
	list_mov.set(iter_mov, 0, num, 1, fech, 2, fact, 3, conc, 4, imp_s, 5, money_s);
	
	return money;
}

void aux_lista_af(Gtk.ListStore list, string[] values, string table)
{
	Gtk.TreeIter iter;
	list.prepend(out iter);	
	
	string num = "";
	string date_out = "";
	string cc = values[0];
	string name = values[1];
	string surname1 = values[2];
	string surname2 = values[3];
	string birth = values[4];
	string phone = values[5];
	string mail = values[6];
	string address = values[7];
	string zone = values[8];
	string obs = values[9];
	string ocu = values[10];
	string sec = values[11];
	string center = values[12];
	string address_c = values[13];
	string obs_c = values[14];
	string date = values[15];
	string cot_s = values[16];
	if(cot_s != "exento/a")
	{
		string year = cot_s[0:4];
		string month = mes_texto(int.parse(cot_s[5:7]));
		cot_s = @"$month $year";
	}
	if(cc.char_count() > 3)
	{
		string aux = @"$cc ";
		num = aux[-5:-1];
	}
	if(table == "bajas")
	{
		date_out = values[17];			
	}			
	list.set(iter, 0, num, 1, cc, 2, surname1, 3, surname2, 4, name, 5, birth, 6, phone, 7, mail, 8, address, 9, zone, 10, obs, 11, ocu, 12, sec, 13, center, 14, address_c, 15, obs_c, 16, date, 17, cot_s, 18, date_out);
}
