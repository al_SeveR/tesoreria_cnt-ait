// Constante
const int NUM_MAX = 999999999;

bool crear_caja(Gtk.ApplicationWindow window)
{
	bool creada = false;
	Gtk.Dialog dialog = new Gtk.Dialog.with_buttons("Crear caja", window, Gtk.DialogFlags.MODAL, "Cancelar", Gtk.ResponseType.CANCEL, "Aceptar", Gtk.ResponseType.OK);
	dialog.set_resizable(false);
	var content_area = dialog.get_content_area();
	content_area.set_property("margin", 10);
	content_area.set_property("spacing", 10);
	
	Gtk.Grid grid = new Gtk.Grid();
	grid.set_column_spacing(10);
	grid.set_row_spacing(10);
		
	Gtk.Label label_init = new Gtk.Label("Saldo inicial (€):");
	label_init.halign = Gtk.Align.END;
	grid.attach(label_init, 0, 0, 1, 1);
	Gtk.SpinButton init = new Gtk.SpinButton.with_range(-NUM_MAX,NUM_MAX,1);
	init.set_digits(2);
	init.set_value(0);
	grid.attach(init, 1, 0, 1, 1);
	
	Gtk.Label label_name = new Gtk.Label("Nombre:");
	label_name.halign = Gtk.Align.END;
	grid.attach(label_name, 0, 1, 1, 1);
	Gtk.Entry name = new Gtk.Entry();
	name.max_length = 30;
	grid.attach(name, 1, 1, 1, 1);
	
	content_area.add(grid);
	content_area.show_all();
	name.grab_focus();
	
	while(true)
	{
		var result = dialog.run();
		if(result == Gtk.ResponseType.OK) 
		{
			string name_cash = name.get_text();
			if(name_cash != "")
			{
				var exist = false;
	
				db.exec("SELECT name FROM sqlite_master", (n_columns, values, column_names) => 
				{
					if(values[0] == name_cash)
					{
						exist = true;
					}
					return 0;
				}, null);
				
				if(exist == false)
				{
					if(db.exec (@"CREATE TABLE $name_cash (num INTEGER PRIMARY KEY, fecha datetime, factura text, concepto text, euros double)", null, null) != Sqlite.OK)
					{
						mensaje_pop(name,"Nombre no válido.");
					}
					else
					{
						var val = init.get_value();
						var now = new DateTime.now_local();
						var time = now.format("%Y-%m-%d %H-%M-%S");
						db.exec (@"INSERT INTO $name_cash (fecha, factura, concepto, euros) VALUES ('$time', '', 'Saldo inicial', $val)");
						creada = true;
						break;
					}
				}
				else
				{
					mensaje_pop(name,"Ya existe una caja con ese nombre.");
				}
			}
			else
			{
				mensaje_pop(name,"Es necesario dar un nombre a la caja.");
			}
		}
		else
		{
			break;
		}
	}
	dialog.destroy();
	return (creada);
}

void eliminar_caja(Gtk.ApplicationWindow window, Gtk.ComboBox combo_ca)
{
	Gtk.Dialog dialog = new Gtk.Dialog.with_buttons("Eliminar caja", window, Gtk.DialogFlags.MODAL, "Cancelar", Gtk.ResponseType.CANCEL, "Aceptar", Gtk.ResponseType.OK);
	dialog.set_resizable(false);
	var content_area = dialog.get_content_area();
	content_area.set_property("margin", 10);
	content_area.set_property("spacing", 10);
	
	Gtk.Grid grid = new Gtk.Grid();
	grid.set_column_spacing(10);
	grid.set_row_spacing(10);
	
	Gtk.Label label_or = new Gtk.Label("Caja a eliminar:");
	label_or.halign = Gtk.Align.END;
	grid.attach(label_or, 0, 0, 1, 1);
	Gtk.TreeIter iter;
	combo_ca.get_active_iter(out iter);
	var model = combo_ca.get_model();
	Value name;
	model.get_value(iter, 0, out name);
	var name_s = (string) name;
	Gtk.Label label_name = new Gtk.Label(name_s);
	grid.attach(label_name, 1, 0, 1, 1);
	
	Gtk.Label label_des = new Gtk.Label("Destino:");
	label_des.halign = Gtk.Align.END;
	grid.attach(label_des, 0, 1, 1, 1);
	Gtk.ListStore list = new Gtk.ListStore(1, typeof(string));	
	if (cajas > 1)
	{
		db.exec(@"SELECT name FROM sqlite_master WHERE name != '$name_s' AND name != 'altas' AND name != 'bajas' AND name != 'cotizaciones'", (n_columns, values, column_names) => 
		{
			list.append(out iter);
			list.set(iter, 0, values[0]);
			return 0;
		}, null);
	} 
	Gtk.ComboBox combo = new Gtk.ComboBox.with_model(list);
	Gtk.CellRendererText cell = new Gtk.CellRendererText ();
	combo.pack_start(cell,false);
	combo.set_attributes (cell, "text", 0);
	combo.set_active(-1);
	grid.attach(combo, 1, 1, 1, 1);
	
	content_area.add(grid);
	content_area.show_all();
	
	while(true)
	{
		var result = dialog.run();
		if(result == Gtk.ResponseType.OK) 
		{
			combo.get_active_iter(out iter);
			model = combo.get_model();
			model.get_value(iter, 0, out name);
			var name_s2 = (string) name;
			if(combo.get_active() != -1)
			{	
				string money = "";
				var now = new DateTime.now_local();
				var time = now.format("%Y-%m-%d %H-%M-%S");
				
				db.exec(@"SELECT SUM(euros) FROM '$name_s'", (n_columns, values, column_names) => 
				{
					money = values[0];
					return 0;
				}, null);	
				db.exec(@"DROP TABLE $name_s", null, null);
				db.exec(@"INSERT INTO $name_s2 (fecha, factura, concepto, euros) VALUES ('$time', '', '$name_s eliminada', '$money')", null, null);
				actualiza_combo_ca(window, combo_ca);
				cajas--;
				break;
			}
			else
			{
					mensaje_pop(combo, "Es necesario seleccionar una opción.");
			}
		}
		else
		{
			break;
		}
	}	
	 dialog.destroy();
}

bool cuadrar(Gtk.ApplicationWindow window, Gtk.ComboBox combo_ca)
{
	bool cua = false;
	double money = 0.0;
	Gtk.Dialog dialog = new Gtk.Dialog.with_buttons("Cuadrar caja", window, Gtk.DialogFlags.MODAL, "Cancelar", Gtk.ResponseType.CANCEL, "Aceptar", Gtk.ResponseType.OK);
	dialog.set_resizable(false);
	var content_area = dialog.get_content_area();
	content_area.set_property("margin", 10);
	content_area.set_property("spacing", 10);
	
	Gtk.Grid grid = new Gtk.Grid();
	grid.set_column_spacing(10);
	grid.set_row_spacing(10);
	
	Gtk.TreeIter iter;
	combo_ca.get_active_iter(out iter);
	var model = combo_ca.get_model();
	Value name;
	model.get_value(iter, 0, out name);
	var name_s = (string) name;
	Gtk.Label label_name = new Gtk.Label(name_s);
	grid.attach(label_name, 0, 0, 6, 1);
	
	Gtk.Label label_500 = new Gtk.Label("500 €:");
	label_500.halign = Gtk.Align.END;
	grid.attach(label_500, 0, 1, 1, 1);
	Gtk.SpinButton quan_500 = new Gtk.SpinButton.with_range(0,NUM_MAX,1);
	quan_500.set_value(0);
	grid.attach(quan_500, 1, 1, 1, 1);
	
	Gtk.Label label_200 = new Gtk.Label("200 €:");
	label_200.halign = Gtk.Align.END;
	grid.attach(label_200, 2, 1, 1, 1);
	Gtk.SpinButton quan_200 = new Gtk.SpinButton.with_range(0,NUM_MAX,1);
	quan_200.set_value(0);
	grid.attach(quan_200, 3, 1, 1, 1);
		
	Gtk.Label label_100 = new Gtk.Label("100 €:");
	label_100.halign = Gtk.Align.END;
	grid.attach(label_100, 4, 1, 1, 1);
	Gtk.SpinButton quan_100 = new Gtk.SpinButton.with_range(0,NUM_MAX,1);
	quan_100.set_value(0);
	grid.attach(quan_100, 5, 1, 1, 1);
		
	Gtk.Label label_50 = new Gtk.Label("50 €:");
	label_50.halign = Gtk.Align.END;
	grid.attach(label_50, 0, 2, 1, 1);
	Gtk.SpinButton quan_50 = new Gtk.SpinButton.with_range(0,NUM_MAX,1);
	quan_50.set_value(0);
	grid.attach(quan_50, 1, 2, 1, 1);

	Gtk.Label label_20 = new Gtk.Label("20 €:");
	label_20.halign = Gtk.Align.END;
	grid.attach(label_20, 2, 2, 1, 1);
	Gtk.SpinButton quan_20 = new Gtk.SpinButton.with_range(0,NUM_MAX,1);
	quan_20.set_value(0);
	grid.attach(quan_20, 3, 2, 1, 1);

	Gtk.Label label_10 = new Gtk.Label("10 €:");
	label_10.halign = Gtk.Align.END;
	grid.attach(label_10, 4, 2, 1, 1);
	Gtk.SpinButton quan_10 = new Gtk.SpinButton.with_range(0,NUM_MAX,1);
	quan_10.set_value(0);
	grid.attach(quan_10, 5, 2, 1, 1);
	
	Gtk.Label label_5 = new Gtk.Label("5 €:");
	label_5.halign = Gtk.Align.END;
	grid.attach(label_5, 0, 3, 1, 1);
	Gtk.SpinButton quan_5 = new Gtk.SpinButton.with_range(0,NUM_MAX,1);
	quan_5.set_value(0);
	grid.attach(quan_5, 1, 3, 1, 1);
	
	Gtk.Label label_2 = new Gtk.Label("2 €:");
	label_2.halign = Gtk.Align.END;
	grid.attach(label_2, 2, 3, 1, 1);
	Gtk.SpinButton quan_2 = new Gtk.SpinButton.with_range(0,NUM_MAX,1);
	quan_2.set_value(0);
	grid.attach(quan_2, 3, 3, 1, 1);

	Gtk.Label label_1 = new Gtk.Label("1 €:");
	label_1.halign = Gtk.Align.END;
	grid.attach(label_1, 4, 3, 1, 1);
	Gtk.SpinButton quan_1 = new Gtk.SpinButton.with_range(0,NUM_MAX,1);
	quan_1.set_value(0);
	grid.attach(quan_1, 5, 3, 1, 1);
	
	Gtk.Label label_05 = new Gtk.Label("0,50 €:");
	label_05.halign = Gtk.Align.END;
	grid.attach(label_05, 0, 4, 1, 1);
	Gtk.SpinButton quan_05 = new Gtk.SpinButton.with_range(0,NUM_MAX,1);
	quan_05.set_value(0);
	grid.attach(quan_05, 1, 4, 1, 1);
	
	Gtk.Label label_02 = new Gtk.Label("0,20 €:");
	label_02.halign = Gtk.Align.END;
	grid.attach(label_02, 2, 4, 1, 1);
	Gtk.SpinButton quan_02 = new Gtk.SpinButton.with_range(0,NUM_MAX,1);
	quan_02.set_value(0);
	grid.attach(quan_02, 3, 4, 1, 1);

	Gtk.Label label_01 = new Gtk.Label("0,10 €:");
	label_01.halign = Gtk.Align.END;
	grid.attach(label_01, 4, 4, 1, 1);
	Gtk.SpinButton quan_01 = new Gtk.SpinButton.with_range(0,NUM_MAX,1);
	quan_01.set_value(0);
	grid.attach(quan_01, 5, 4, 1, 1);
	
	Gtk.Label label_005 = new Gtk.Label("0,05 €:");
	label_005.halign = Gtk.Align.END;
	grid.attach(label_005, 0, 5, 1, 1);
	Gtk.SpinButton quan_005 = new Gtk.SpinButton.with_range(0,NUM_MAX,1);
	quan_005.set_value(0);
	grid.attach(quan_005, 1, 5, 1, 1);
	
	Gtk.Label label_002 = new Gtk.Label("0,02 €:");
	label_002.halign = Gtk.Align.END;
	grid.attach(label_002, 2, 5, 1, 1);
	Gtk.SpinButton quan_002 = new Gtk.SpinButton.with_range(0,NUM_MAX,1);
	quan_002.set_value(0);
	grid.attach(quan_002, 3, 5, 1, 1);

	Gtk.Label label_001 = new Gtk.Label("0,01 €:");
	label_001.halign = Gtk.Align.END;
	grid.attach(label_001, 4, 5, 1, 1);
	Gtk.SpinButton quan_001 = new Gtk.SpinButton.with_range(0,NUM_MAX,1);
	quan_001.set_value(0);
	grid.attach(quan_001, 5, 5, 1, 1);		
	
	Gtk.Button sum = new Gtk.Button.with_label("Sumar");
	grid.attach(sum, 3, 6, 1, 1);
	
	Gtk.Label label_tot = new Gtk.Label("Total (€): ");
	label_tot.halign = Gtk.Align.END;
	grid.attach(label_tot, 2, 7, 1, 1);	
	Gtk.SpinButton quan_tot = new Gtk.SpinButton.with_range(0,NUM_MAX,1);
	quan_tot.set_value(0);
	quan_tot.set_digits(2);
    quan_tot.set_numeric(false);
	grid.attach(quan_tot, 3, 7, 1, 1);	
					
	content_area.add(grid);
	content_area.show_all();
	sum.grab_focus();
	
	sum.clicked.connect ( () =>
	{
		quan_tot.set_value(quan_500.get_value()*500+quan_200.get_value()*200+quan_100.get_value()*100+quan_50.get_value()*50+quan_20.get_value()*20+quan_10.get_value()*10+quan_5.get_value()*5+quan_2.get_value()*2+quan_1.get_value()*1+quan_05.get_value()*0.5+quan_02.get_value()*0.2+quan_01.get_value()*0.1+quan_005.get_value()*0.05+quan_002.get_value()*0.02+quan_001.get_value()*0.01);
	});
	
	var result = dialog.run();
	if(result == Gtk.ResponseType.OK) 
	{
		db.exec(@"SELECT SUM(euros) FROM $name_s", (n_columns, values, column_names) => 
		{
			if(values[0] != null)
			{	
				money = double.parse(values[0]);
			}
		return 0;
		}, null);	
		var dif = quan_tot.get_value() - money;	
		var now = new DateTime.now_local();
		var time = now.format("%Y-%m-%d %H-%M-%S");
		db.exec(@"INSERT INTO $name_s (fecha, factura, concepto, euros) VALUES ('$time', '', 'Cuadre de caja', $dif)");
		cua = true;
	}
	dialog.destroy();
	return cua;
}

bool movimiento(Gtk.ApplicationWindow window, Gtk.ComboBox combo_ca)
{
	bool mov = false;
	Gtk.Dialog dialog = new Gtk.Dialog.with_buttons("Movimiento", window, Gtk.DialogFlags.MODAL, "Cancelar", Gtk.ResponseType.CANCEL, "Aceptar", Gtk.ResponseType.OK);
	//dialog.set_resizable(false);
	var content_area = dialog.get_content_area();
	content_area.set_property("margin", 10);
	content_area.set_property("spacing", 10);
	
	Gtk.Grid grid = new Gtk.Grid();
	grid.set_column_spacing(10);
	grid.set_row_spacing(10);
	
	Gtk.TreeIter iter;
	combo_ca.get_active_iter(out iter);
	var model = combo_ca.get_model();
	Value name;
	model.get_value(iter, 0, out name);
	Gtk.Label label_name = new Gtk.Label((string)name);
	label_name.set_markup(@"<big>$(label_name.get_text())</big>");
	grid.attach(label_name, 0, 0, 2, 1);
	
	Gtk.Label label_dir = new Gtk.Label("Entrada / Salida:");
	label_dir.halign = Gtk.Align.END;
	grid.attach(label_dir, 0, 1, 1, 1);
	
	Gtk.ListStore list = new Gtk.ListStore(1, typeof(string));
	list.append(out iter);
	list.set(iter, 0, "Entrada");
	list.append(out iter);
	list.set(iter, 0, "Salida");	
	Gtk.ComboBox combo = new Gtk.ComboBox.with_model(list);
	Gtk.CellRendererText cell = new Gtk.CellRendererText ();
	combo.pack_start(cell,false);
	combo.set_attributes (cell, "text", 0);
	combo.set_active(-1);
	grid.attach(combo, 1, 1, 1, 1);	
	
	Gtk.Label label_quan = new Gtk.Label("Cantidad (€):");
	label_quan.halign = Gtk.Align.END;
	grid.attach(label_quan, 0, 2, 1, 1);
	Gtk.SpinButton quantity = new Gtk.SpinButton.with_range(0,NUM_MAX,1);
	quantity.set_digits(2);
	quantity.set_value(0);
    quantity.set_numeric(false);
	grid.attach(quantity, 1, 2, 1, 1);
	
	Gtk.Label label_inv = new Gtk.Label("Factura:");
	label_inv.halign = Gtk.Align.END;
	grid.attach(label_inv, 0, 3, 1, 1);
	Gtk.Entry invoice = new Gtk.Entry();
	invoice.max_length = 30;
	grid.attach(invoice, 1, 3, 1, 1);
	
	Gtk.Label label_con = new Gtk.Label("Concepto:");
	label_con.halign = Gtk.Align.END;
	grid.attach(label_con, 0, 4, 1, 1);
	Gtk.Entry concept = new Gtk.Entry();
	concept.max_length = 140;
	grid.attach(concept, 1, 4, 1, 1);
    
    Gtk.Label label_date = new Gtk.Label("Fecha:");
	label_date.halign = Gtk.Align.END;
	grid.attach(label_date, 0, 5, 1, 1);
	Gtk.Entry date = new Gtk.Entry();
	date.max_length = 10;
	date.set_property("width-chars", 12);
	date.set_placeholder_text("dd/mm/aaaa");
	date.set_icon_from_icon_name(0, "x-office-calendar-symbolic");		
	date.icon_press.connect (() =>
	{
		date.grab_focus();
		calendario(date);
	});
    texto_desde_hasta(date, false);
    grid.attach(date, 1, 5, 1, 1);
	
	content_area.add(grid);
	content_area.show_all();
	
	while(true)
	{
		var result = dialog.run();
		if(result == Gtk.ResponseType.OK) 
		{
			if(combo.get_active() != -1)
			{
				var label_name_s = label_name.get_text();
				var invoice_s = invoice.get_text();
				var concept_s = concept.get_text();
				var quantity_i = quantity.get_value();
				if(combo.get_active() == 1)
				{
					quantity_i = -quantity_i;
				}
                string time = formatea_texto(date, 2);
                if(time != "")
                {
                    db.exec(@"INSERT INTO $label_name_s (fecha, factura, concepto, euros) VALUES ('$time', '$invoice_s', '$concept_s', $quantity_i)");
                    mov = true;
                    break;
                }
                else
                {
                    mensaje_pop(date, "Fecha incorrecta.");
                }                
			}
			else
			{
				mensaje_pop(combo, "Es necesario seleccionar una opción.");
			}
		}
		else
		{
			break;
		}
	}
	dialog.destroy();
	return mov;
}

bool transferencia(Gtk.ApplicationWindow window, Gtk.ComboBox combo_ca)
{
	bool trans = false;
	Gtk.Dialog dialog = new Gtk.Dialog.with_buttons("Transferencia", window, Gtk.DialogFlags.MODAL, "Cancelar", Gtk.ResponseType.CANCEL, "Aceptar", Gtk.ResponseType.OK);
	//dialog.set_resizable(false);
	var content_area = dialog.get_content_area();
	content_area.set_property("margin", 10);
	content_area.set_property("spacing", 10);
	
	Gtk.Grid grid = new Gtk.Grid();
	grid.set_column_spacing(10);
	grid.set_row_spacing(10);
	
	Gtk.Label label_or = new Gtk.Label("Origen:");
	label_or.halign = Gtk.Align.END;
	grid.attach(label_or, 0, 0, 1, 1);
	Gtk.TreeIter iter;
	combo_ca.get_active_iter(out iter);
	var model = combo_ca.get_model();
	Value name;
	model.get_value(iter, 0, out name);
	var name_s = (string) name;
	Gtk.Label label_name = new Gtk.Label(name_s);
	label_name.halign = Gtk.Align.START;
	grid.attach(label_name, 1, 0, 1, 1);
	
	Gtk.Label label_quan = new Gtk.Label("Cantidad (€):");
	label_quan.halign = Gtk.Align.END;
	grid.attach(label_quan, 0, 1, 1, 1);
	Gtk.SpinButton quantity = new Gtk.SpinButton.with_range(0,NUM_MAX,1);
	quantity.set_digits(2);
	quantity.set_value(0);
    quantity.set_numeric(false);
	grid.attach(quantity, 1, 1, 1, 1);
	
	Gtk.Label label_des = new Gtk.Label("Destino:");
	label_des.halign = Gtk.Align.END;
	grid.attach(label_des, 0, 2, 1, 1);
	Gtk.ListStore list = new Gtk.ListStore(1, typeof(string));
	if (cajas > 1)
	{
		db.exec(@"SELECT name FROM sqlite_master WHERE name != '$name_s' AND name != 'altas' AND name != 'bajas' AND name != 'cotizaciones'", (n_columns, values, column_names) => 
		{
			list.append(out iter);
			list.set(iter, 0, values[0]);
			return 0;
		}, null);
	} 
	Gtk.ComboBox combo = new Gtk.ComboBox.with_model(list);
	Gtk.CellRendererText cell = new Gtk.CellRendererText ();
	combo.pack_start(cell,false);
	combo.set_attributes (cell, "text", 0);
	combo.set_active(-1);
	grid.attach(combo, 1, 2, 1, 1);	
    
    Gtk.Label label_date = new Gtk.Label("Fecha:");
	label_date.halign = Gtk.Align.END;
	grid.attach(label_date, 0, 3, 1, 1);
	Gtk.Entry date = new Gtk.Entry();
	date.max_length = 10;
	date.set_property("width-chars", 12);
	date.set_placeholder_text("dd/mm/aaaa");
	date.set_icon_from_icon_name(0, "x-office-calendar-symbolic");		
	date.icon_press.connect (() =>
	{
		date.grab_focus();
		calendario(date);
	});
    texto_desde_hasta(date, false);
    grid.attach(date, 1, 3, 1, 1);
		
	content_area.add(grid);
	content_area.show_all();
	combo.grab_focus();
	
	while(true)
	{
		var result = dialog.run();
		if(result == Gtk.ResponseType.OK) 
		{
			if(combo.get_active() != -1)
			{
				combo.get_active_iter(out iter);
				model = combo.get_model();
				model.get_value(iter, 0, out name);
				var name_s2 = (string) name;
				var quantity_i = quantity.get_value();
                string time = formatea_texto(date, 2);
                if(time != "")
                {
                    db.exec(@"INSERT INTO $name_s (fecha, factura, concepto, euros) VALUES ('$time', '', 'Transferencia a $name_s2', -$quantity_i)");
                    db.exec(@"INSERT INTO $name_s2 (fecha, factura, concepto, euros) VALUES ('$time', '', 'Transferencia desde $name_s', $quantity_i)");
                    trans = true;
                    break;
                }
                else
                {
                    mensaje_pop(date, "Fecha incorrecta.");
                }
			}
			else
			{
				mensaje_pop(combo, "Es necesario seleccionar una opción.");
			}
		}
		else
		{
			break;
		}
	}
	dialog.destroy();
	return trans;
}

void exportar_ca (Gtk.ApplicationWindow window, Gtk.ListStore list_mov, Gtk.ComboBox combo_ca, Gtk.Entry since, Gtk.Entry to, Gtk.ComboBox combo_fil)
{
	var dialog = new Gtk.FileChooserDialog("Guardar como...", window, Gtk.FileChooserAction.SAVE, "Cancelar", Gtk.ResponseType.CANCEL, "Guardar", Gtk.ResponseType.OK);
	dialog.set_modal(true);
	dialog.set_do_overwrite_confirmation(true);
	dialog.set_current_name("extracto.csv");
	var result = dialog.run();
	if(result == Gtk.ResponseType.OK) 
	{
		double money = 0.0;
	
		var texto_since = formatea_texto(since, 0);
		var texto_to = formatea_texto(to, 1);
	
		if(texto_since != "" && texto_to != "")
		{
			var file = dialog.get_file();
			try
			{
				if(file.query_exists())
				{
					file.delete();
				}
				var dos = new DataOutputStream (file.create (FileCreateFlags.REPLACE_DESTINATION));
				
				char[] buffer = new char[20];

				Gtk.TreeIter iter_pre;
				combo_ca.get_active_iter(out iter_pre);
				var model = combo_ca.get_model();
				Value name;
				model.get_value(iter_pre, 0, out name);
				var name_s = (string) name;
		
				string fech_since = formatea_texto_esp(since);
				string fech_to = formatea_texto_esp(to);
                
                if(search_text == "")
				{
                    if(combo_fil.get_active() == 0)
                    {
                        dos.put_string(@"Caja: $name_s;Desde: $fech_since;Hasta: $fech_to\n\n");
                        dos.put_string("Número;Fecha;Factura;Concepto;Importe;Saldo\n");
                    }
                    else if(combo_fil.get_active() == 1)
                    {
                        dos.put_string(@"Caja: $name_s;Desde: $fech_since;Hasta: $fech_to;Solo entradas\n\n");
                        dos.put_string("Número;Fecha;Factura;Concepto;Importe\n");
                    }	
                    else
                    {
                        dos.put_string(@"Caja: $name_s;Desde: $fech_since;Hasta: $fech_to;Solo salidas\n\n");
                        dos.put_string("Número;Fecha;Factura;Concepto;Importe\n");
                    }	
                }
                else
                {
                    if(combo_fil.get_active() == 0)
                    {
                        dos.put_string(@"Caja: $name_s;Desde: $fech_since;Hasta: $fech_to; Búsqueda: $search_text\n\n");
                        dos.put_string("Número;Fecha;Factura;Concepto;Importe\n");
                    }
                    else if(combo_fil.get_active() == 1)
                    {
                        dos.put_string(@"Caja: $name_s;Desde: $fech_since;Hasta: $fech_to;Solo entradas; Búsqueda: $search_text\n\n");
                        dos.put_string("Número;Fecha;Factura;Concepto;Importe\n");
                    }	
                    else
                    {
                        dos.put_string(@"Caja: $name_s;Desde: $fech_since;Hasta: $fech_to;Solo salidas; Búsqueda: $search_text\n\n");
                        dos.put_string("Número;Fecha;Factura;Concepto;Importe\n");
                    }	 
                }						
				db.exec(@"SELECT SUM(euros) FROM $name_s WHERE fecha < '$texto_since'", (n_columns, values, column_names) => 
				{
					if(values[0] != null)
					{	
						money = double.parse(values[0]);
					}
					return 0;
				}, null);	
				db.exec(@"SELECT * FROM $name_s WHERE fecha >= '$texto_since' AND fecha <= '$texto_to' AND (num LIKE '%$search_text%' OR fecha LIKE '%$search_text%' OR factura LIKE '%$search_text%' OR concepto LIKE '%$search_text%' OR euros LIKE '%$search_text%') ORDER BY fecha", (n_columns, values, column_names) => 
				{	
					string num = values[0];
					string fech_total = values[1];
					string year = fech_total[0:4];
					string month = fech_total[5:7];
					string day = fech_total[8:10];
					string fech = @"$day/$month/$year";			
				
					string fact = values[2];
					string conc = values[3];
					double imp = double.parse(values[4]);
		
					money = money + imp;
					string imp_s = (imp.format (buffer, "%.2f")).replace(".",",") + " €";
					string money_s = (money.format (buffer, "%.2f")).replace(".",",") + " €";
					try
					{
						if(combo_fil.get_active() == 0 && search_text == "")
						{
							dos.put_string(@"$num;$fech;$fact;$conc;$imp_s;$money_s\n");
						}
                        else if (combo_fil.get_active() == 0 && search_text != "")
                        {
                            dos.put_string(@"$num;$fech;$fact;$conc;$imp_s\n");
                        }
						else if(combo_fil.get_active() == 1 && imp > 0)
						{
							dos.put_string(@"$num;$fech;$fact;$conc;$imp_s\n");
						}
						else if(combo_fil.get_active() == 2 && imp < 0)
						{
							dos.put_string(@"$num;$fech;$fact;$conc;$imp_s\n");
						}						
					}
					catch(Error e3)
					{
						stderr.printf("%s\n", e3.message);
					}
					return 0;
				}, null);
			}
			catch(Error e1)
			{
				stderr.printf("%s\n", e1.message);
			}
		}
		else
		{
			var file = dialog.get_file();
			try
			{
				if(file.query_exists())
				{
					file.delete();
				}
				var dos = new DataOutputStream (file.create (FileCreateFlags.REPLACE_DESTINATION));
				dos.put_string(@"Error en las fechas. Trata de exportar de nuevo.\n");
			}
			catch(Error e1)
			{
				stderr.printf("%s\n", e1.message);
			}			
		}
	}	
	dialog.destroy();
}

void exportar_af (Gtk.ApplicationWindow window, Gtk.ComboBox combo_af)
{
	var dialog = new Gtk.FileChooserDialog("Guardar como...", window, Gtk.FileChooserAction.SAVE, "Cancelar", Gtk.ResponseType.CANCEL, "Guardar", Gtk.ResponseType.OK);
	dialog.set_modal(true);
	dialog.set_do_overwrite_confirmation(true);
	dialog.set_current_name("afiliacion.csv");
	var result = dialog.run();
	if(result == Gtk.ResponseType.OK) 
	{
		var file = dialog.get_file();
		try
		{
			if(file.query_exists())
			{
				file.delete();
			}
			var dos = new DataOutputStream (file.create (FileCreateFlags.REPLACE_DESTINATION));	
			string table;
			if(combo_af.get_active() == 0)
			{
				table = "altas";
				dos.put_string("Altas;");
			}
			else
			{
				table = "bajas";
				dos.put_string("Bajas;");
			}
			bool checks[19];
			if(search_text == "")
			{
				if(filters_s == "all")	
				{
					dos.put_string("\n\n");
					checks = genera_checks(dos);
					db.exec(@"SELECT * FROM $table ORDER BY apellido1 DESC, apellido2 DESC, nombre DESC", (n_columns, values, column_names) => 
					{
						aux_exportar_af(checks, values, table, dos);
						return 0;
					}, null);	
				}
				else if(filters_s == "exempt")
				{	
					dos.put_string("Exentos\n\n");
					checks = genera_checks(dos);
					db.exec(@"SELECT * FROM $table WHERE cotizado = 'exento/a' ORDER BY apellido1 DESC, apellido2 DESC, nombre DESC", (n_columns, values, column_names) => 
					{
						aux_exportar_af(checks, values, table, dos);
						return 0;
					}, null);		
				}
				else if(filters_s == "debtors")
				{
					dos.put_string(@"Deudores $months_fil meses\n\n");
					checks = genera_checks(dos);
					var now = new DateTime.now_local();
					var month_now = int.parse(now.format("%m"));
					var year_i = int.parse(now.format("%Y"));
					var month_i = month_now - months_fil;
					if(month_i < 1)
					{
						year_i--;
						month_i = month_i + 12;
					}
					string cot_req;
					if(month_i < 10)
					{
						cot_req = @"$year_i-0$month_i";
					}
					else
					{
						cot_req = @"$year_i-$month_i";
					}
					db.exec(@"SELECT * FROM $table WHERE cotizado <= '$cot_req' ORDER BY apellido1 DESC, apellido2 DESC, nombre DESC", (n_columns, values, column_names) => 
					{
						aux_exportar_af(checks, values, table, dos);
						return 0;
					}, null);			
				}	
			}
			else
			{
				string search_date_out = "";
				if(table == "bajas")
				{
					search_date_out = "OR fechabaja LIKE '%$search_text%'";
				}
				if(filters_s == "all")
				{	
					dos.put_string(@"Búsqueda: $search_text;");
					dos.put_string("\n\n");
					checks = genera_checks(dos);
					db.exec(@"SELECT * FROM $table WHERE cc LIKE '%$search_text%' OR nombre LIKE '%$search_text%' OR apellido1 LIKE '%$search_text%' OR apellido2 LIKE '%$search_text%' OR fnacimiento LIKE '%$search_text%' OR telefono LIKE '%$search_text%' OR email LIKE '%$search_text%' OR dir LIKE '%$search_text%' OR zona LIKE '%$search_text%' OR obs LIKE '%$search_text%' OR ocu LIKE '%$search_text%' OR sec LIKE '%$search_text%' OR centro LIKE '%$search_text%' OR dirc LIKE '%$search_text%' OR obsc LIKE '%$search_text%' OR afiliado LIKE '%$search_text%' $search_date_out ORDER BY apellido1 DESC, apellido2 DESC, nombre DESC", (n_columns, values, column_names) => 
					{
						aux_exportar_af(checks, values, table, dos);
						return 0;
					}, null);		
				}
				else if(filters_s == "exempt")
				{
					dos.put_string(@"Búsqueda: $search_text;");
					dos.put_string("Exentos\n\n");
					checks = genera_checks(dos);
					db.exec(@"SELECT * FROM $table WHERE cotizado = 'exento/a' AND (cc LIKE '%$search_text%' OR nombre LIKE '%$search_text%' OR apellido1 LIKE '%$search_text%' OR apellido2 LIKE '%$search_text%' OR fnacimiento LIKE '%$search_text%' OR telefono LIKE '%$search_text%' OR email LIKE '%$search_text%' OR dir LIKE '%$search_text%' OR zona LIKE '%$search_text%' OR obs LIKE '%$search_text%' OR ocu LIKE '%$search_text%' OR sec LIKE '%$search_text%' OR centro LIKE '%$search_text%' OR dirc LIKE '%$search_text%' OR obsc LIKE '%$search_text%' OR afiliado LIKE '%$search_text%' $search_date_out) ORDER BY apellido1 DESC, apellido2 DESC, nombre DESC", (n_columns, values, column_names) =>
					{
						aux_exportar_af(checks, values, table, dos);
						return 0;
					}, null);			
				}
				else if(filters_s == "debtors")
				{
					dos.put_string(@"Búsqueda: $search_text;");
					dos.put_string(@"Deudores $months_fil meses\n\n");
					checks = genera_checks(dos);
					var now = new DateTime.now_local();
					var month_now = int.parse(now.format("%m"));
					var year_i = int.parse(now.format("%Y"));
					var month_i = month_now - months_fil;
					if(month_i < 1)
					{
						year_i--;
						month_i = month_i + 12;
					}
					string cot_req;
					if(month_i < 10)
					{
						cot_req = @"$year_i-0$month_i";
					}
					else
					{
						cot_req = @"$year_i-$month_i";
					}
					db.exec(@"SELECT * FROM $table WHERE cotizado <= '$cot_req' AND (cc LIKE '%$search_text%' OR nombre LIKE '%$search_text%' OR apellido1 LIKE '%$search_text%' OR apellido2 LIKE '%$search_text%' OR fnacimiento LIKE '%$search_text%' OR telefono LIKE '%$search_text%' OR email LIKE '%$search_text%' OR dir LIKE '%$search_text%' OR zona LIKE '%$search_text%' OR obs LIKE '%$search_text%' OR ocu LIKE '%$search_text%' OR sec LIKE '%$search_text%' OR centro LIKE '%$search_text%' OR dirc LIKE '%$search_text%' OR obsc LIKE '%$search_text%' OR afiliado LIKE '%$search_text%' $search_date_out) ORDER BY apellido1 DESC, apellido2 DESC, nombre DESC", (n_columns, values, column_names) =>
					{
						aux_exportar_af(checks, values, table, dos);
						return 0;
					}, null);				
				}
			}	
		}
        catch(Error e1)
		{
			stderr.printf("%s\n", e1.message);
		}
    }
	dialog.destroy();
}

void saldos (Gtk.ApplicationWindow window)
{
	Gtk.Dialog dialog = new Gtk.Dialog.with_buttons("Saldos", window, Gtk.DialogFlags.MODAL, "Cerrar", Gtk.ResponseType.CLOSE);
	dialog.set_resizable(false);
	var content_area = dialog.get_content_area();
	content_area.set_property("margin", 10);
	content_area.set_property("spacing", 10);
	
	if(cajas > 0)
	{
		char[] buffer = new char[20];
		double total = 0;	
		Gtk.Grid grid = new Gtk.Grid();
		grid.set_column_spacing(10);
		grid.set_row_spacing(10);	
		int i = 0;
		db.exec(@"SELECT name FROM sqlite_master WHERE name != 'altas' AND name != 'bajas' AND name != 'cotizaciones'", (n_columns, values, column_names) => 
		{
			double money = 0;
			string table = values[0];
			db.exec(@"SELECT SUM(euros) FROM $table", (n_columns, values, column_names) => 
			{
				if(values[0] != null)
				{	
					money = double.parse(values[0]);
					total = total + money;
				}
				return 0;
			}, null);	
			string money_s = (money.format (buffer, "%.2f")).replace(".",",") + " €";
			var label_table = new Gtk.Label(@"$table:");
			label_table.halign = Gtk.Align.END;
			grid.attach(label_table, 0, i, 1, 1);
			var label_money = new Gtk.Label(@"$money_s");
			label_money.halign = Gtk.Align.END;
			grid.attach(label_money, 1, i, 1, 1);
			i++;
			return 0;
		}, null);
		Gtk.Separator separator = new Gtk.Separator(Gtk.Orientation.HORIZONTAL);
		grid.attach(separator, 0, i, 2, 1);
		
		string total_s = (total.format (buffer, "%.2f")).replace(".",",") + " €";
		var label_total = new Gtk.Label("Total:");
		label_total.halign = Gtk.Align.END;
		grid.attach(label_total, 0, i+1, 1, 1);
		var label_total_money = new Gtk.Label(@"$total_s");
		label_total_money.halign = Gtk.Align.END;
		grid.attach(label_total_money, 1, i+1, 1, 1);
		content_area.add(grid);
	}
	else
	{
		var label = new Gtk.Label("Aún no existe ninguna caja");
		content_area.add(label);
	}
	content_area.show_all();
	
	dialog.run();
	dialog.destroy();	
}

bool afiliar(Gtk.ApplicationWindow window)
{
	bool afiliado = false;
	Gtk.Dialog dialog = new Gtk.Dialog.with_buttons("Afiliar", window, Gtk.DialogFlags.MODAL, "Cancelar", Gtk.ResponseType.CANCEL, "Aceptar", Gtk.ResponseType.OK);
	dialog.set_resizable(false);
	var content_area = dialog.get_content_area();
	content_area.set_property("margin", 10);
	content_area.set_property("spacing", 10);
	dialog.set_size_request(450, 500);
	
	Gtk.Grid grid = new Gtk.Grid();
	grid.set_column_spacing(10);
	grid.set_row_spacing(10);
	
	Gtk.Label label_name = new Gtk.Label("Nombre:");
	label_name.halign = Gtk.Align.END;
	grid.attach(label_name, 0, 0, 1, 1);
	Gtk.Entry name = new Gtk.Entry();
	name.max_length = 25;
	grid.attach(name, 1, 0, 1, 1);
	
	Gtk.Label label_surname1 = new Gtk.Label("Primer apellido:");
	label_surname1.halign = Gtk.Align.END;
	grid.attach(label_surname1, 0, 1, 1, 1);
	Gtk.Entry surname1 = new Gtk.Entry();
	surname1.max_length = 25;
	grid.attach(surname1, 1, 1, 1, 1);
	
	Gtk.Label label_surname2 = new Gtk.Label("Segundo apellido:");
	label_surname2.halign = Gtk.Align.END;
	grid.attach(label_surname2, 0, 2, 1, 1);
	Gtk.Entry surname2 = new Gtk.Entry();
	surname2.max_length = 25;
	grid.attach(surname2, 1, 2, 1, 1);	
	
	Gtk.Label label_birth = new Gtk.Label("Fecha de nacimiento:");
	label_birth.halign = Gtk.Align.END;
	grid.attach(label_birth, 0, 3, 1, 1);
	Gtk.Entry birth = new Gtk.Entry();
	birth.max_length = 10;
	birth.set_property("width-chars", 12);
	birth.set_placeholder_text("dd/mm/aaaa");
	birth.set_icon_from_icon_name(0, "x-office-calendar-symbolic");			
	birth.icon_press.connect (() =>
	{
		birth.grab_focus();
		calendario(birth);
	});
	grid.attach(birth, 1, 3, 1, 1);
	
	Gtk.Label label_ocu = new Gtk.Label("Ocupación:");
	label_ocu.halign = Gtk.Align.END;
	grid.attach(label_ocu, 0, 4, 1, 1);
	Gtk.Entry ocu = new Gtk.Entry();
	autocompletado(ocu, "ocu");
	ocu.max_length = 50;
	grid.attach(ocu, 1, 4, 1, 1);	
	
	Gtk.Label label_sec = new Gtk.Label("Sección:");
	label_sec.halign = Gtk.Align.END;
	grid.attach(label_sec, 0, 5, 1, 1);
	Gtk.Entry sec = new Gtk.Entry();
	autocompletado(sec, "sec");
	sec.max_length = 50;
	grid.attach(sec, 1, 5, 1, 1);	
	
	Gtk.Label label_cc = new Gtk.Label("Carné Confederal:");
	label_cc.halign = Gtk.Align.END;
	grid.attach(label_cc, 0, 6, 1, 1);
	Gtk.Entry cc = new Gtk.Entry();
	cc.max_length = 20;
	grid.attach(cc, 1, 6, 1, 1);		
	
	Gtk.Label label_mail = new Gtk.Label("e-mail:");
	label_mail.halign = Gtk.Align.END;
	grid.attach(label_mail, 0, 7, 1, 1);
	Gtk.Entry mail = new Gtk.Entry();
	mail.max_length = 50;
	grid.attach(mail, 1, 7, 1, 1);	
	
	Gtk.Label label_phone = new Gtk.Label("Teléfono:");
	label_phone.halign = Gtk.Align.END;
	grid.attach(label_phone, 0, 8, 1, 1);
	Gtk.Entry phone = new Gtk.Entry();
	phone.max_length = 25;
	grid.attach(phone, 1, 8, 1, 1);	
	
	Gtk.Label label_address = new Gtk.Label("Dirección:");
	label_address.halign = Gtk.Align.END;
	grid.attach(label_address, 0, 9, 1, 1);
	Gtk.Entry address = new Gtk.Entry();
	address.max_length = 100;
	grid.attach(address, 1, 9, 1, 1);	
	
	Gtk.Label label_zone = new Gtk.Label("Localidad/Barrio:");
	label_zone.halign = Gtk.Align.END;
	grid.attach(label_zone, 0, 10, 1, 1);
	Gtk.Entry zone = new Gtk.Entry();
	autocompletado(zone, "zona");
	zone.max_length = 50;
	grid.attach(zone, 1, 10, 1, 1);	
	
	Gtk.Label label_obs = new Gtk.Label("Observaciones:");
	label_obs.halign = Gtk.Align.END;
	grid.attach(label_obs, 0, 11, 1, 1);
	Gtk.Entry obs = new Gtk.Entry();
	obs.max_length = 140;
	grid.attach(obs, 1, 11, 1, 1);		
	
	Gtk.Label label_center = new Gtk.Label("Centro de trabajo:");
	label_center.halign = Gtk.Align.END;
	grid.attach(label_center, 0, 12, 1, 1);
	Gtk.Entry center = new Gtk.Entry();
	autocompletado(center, "centro");
	center.max_length = 50;
	grid.attach(center, 1, 12, 1, 1);	
	
	Gtk.Label label_address_c = new Gtk.Label("Dirección del centro:");
	label_address_c.halign = Gtk.Align.END;
	grid.attach(label_address_c, 0, 13, 1, 1);
	Gtk.Entry address_c = new Gtk.Entry();
	autocompletado(address_c, "dirc");
	address_c.max_length = 100;
	grid.attach(address_c, 1, 13, 1, 1);	
	
	Gtk.Label label_obs_c = new Gtk.Label("Observaciones del centro:");
	label_obs_c.halign = Gtk.Align.END;
	grid.attach(label_obs_c, 0, 14, 1, 1);
	Gtk.Entry obs_c = new Gtk.Entry();
	autocompletado(obs_c, "obsc");
	obs_c.max_length = 100;
	grid.attach(obs_c, 1, 14, 1, 1);	
	
	Gtk.Label label_date = new Gtk.Label("Fecha de afiliación:");
	label_date.halign = Gtk.Align.END;
	grid.attach(label_date, 0, 15, 1, 1);
	Gtk.Entry date = new Gtk.Entry();
	date.max_length = 10;
	date.set_property("width-chars", 12);
	date.set_placeholder_text("dd/mm/aaaa");
	date.set_icon_from_icon_name(0, "x-office-calendar-symbolic");	
	texto_desde_hasta(date, false);		
	date.icon_press.connect (() =>
	{
		date.grab_focus();
		calendario(date);
	});
	grid.attach(date, 1, 15, 1, 1);	
	
	Gtk.Label label_exempt = new Gtk.Label("Exento/a:");
	label_exempt.halign = Gtk.Align.END;
	grid.attach(label_exempt, 0, 16, 1, 1);
	Gtk.CheckButton exempt = new Gtk.CheckButton();	
	grid.attach(exempt, 1, 16, 1, 1);
	
	Gtk.Box hbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);	
	Gtk.Label label_cot = new Gtk.Label("Próxima cotización:");
	label_cot.halign = Gtk.Align.END;
	grid.attach(label_cot, 0, 17, 1, 1);
	Gtk.ListStore list = new Gtk.ListStore(1, typeof(string));
	Gtk.TreeIter iter;
	list.append(out iter);
	list.set(iter, 0, "Enero");
	list.append(out iter);
	list.set(iter, 0, "Febrero");	
	list.append(out iter);
	list.set(iter, 0, "Marzo");
	list.append(out iter);
	list.set(iter, 0, "Abril");
	list.append(out iter);
	list.set(iter, 0, "Mayo");
	list.append(out iter);
	list.set(iter, 0, "Junio");	
	list.append(out iter);
	list.set(iter, 0, "Julio");	
	list.append(out iter);
	list.set(iter, 0, "Agosto");
	list.append(out iter);
	list.set(iter, 0, "Septiembre");
	list.append(out iter);
	list.set(iter, 0, "Octubre");
	list.append(out iter);
	list.set(iter, 0, "Noviembre");	
	list.append(out iter);
	list.set(iter, 0, "Diciembre");					
	Gtk.ComboBox combo_month = new Gtk.ComboBox.with_model(list);
	Gtk.CellRendererText cell = new Gtk.CellRendererText ();
	combo_month.pack_start(cell,false);
	combo_month.set_attributes (cell, "text", 0);
	var now = new DateTime.now_local();
	var month = int.parse(now.format("%m")) - 1;	
	combo_month.set_active(month);	
	hbox.add(combo_month);
	Gtk.SpinButton cot_y = new Gtk.SpinButton.with_range(1910,2999,1);
	var year_s = now.format("%Y");
	var year = int.parse(year_s);
	cot_y.set_value(year);
	hbox.add(cot_y);
	grid.attach(hbox, 1, 17, 1, 1);		
	
	exempt.clicked.connect ( () =>
	{
		var sens = combo_month.get_sensitive();
		combo_month.set_sensitive(!sens);
		cot_y.set_sensitive(!sens);	
	});

	Gtk.ScrolledWindow scroll = new Gtk.ScrolledWindow(null,null);	
	scroll.set_propagate_natural_width(true);
	var adj = scroll.get_vadjustment();
	scroll.add(grid);	
	content_area.pack_start(scroll, true, true, 0);
	content_area.show_all();
	
	while(true)
	{
		var result = dialog.run();
		if(result == Gtk.ResponseType.OK) 
		{
			if(cc.get_text() != "")
			{
				var exist = false;
				db.exec("SELECT cc FROM altas", (n_columns, values, column_names) => 
				{
					if(values[0] == cc.get_text())
					{
						exist = true;
					}
					return 0;
				}, null);
				db.exec("SELECT cc FROM bajas", (n_columns, values, column_names) => 
				{
					if(values[0] == cc.get_text())
					{
						exist = true;
					}
					return 0;
				}, null);						
				if(exist == false)
				{
					string cc_s = cc.get_text();
					string name_s = name.get_text();
					string surname1_s = surname1.get_text();
					string surname2_s = surname2.get_text();
					string birth_s = birth.get_text();
					string ocu_s = ocu.get_text();
					string sec_s = sec.get_text();
					string mail_s = mail.get_text();
					string phone_s = phone.get_text();
					string address_s = address.get_text();
					string zone_s = zone.get_text();
					string obs_s = obs.get_text();
					string center_s = center.get_text();
					string address_c_s = address_c.get_text();	
					string obs_c_s = obs_c.get_text();	
					string date_s = date.get_text();								
					string cot_s;
					if(exempt.get_active() == true)
					{
						cot_s = "exento/a"; 
					}
					else
					{
						month = combo_month.get_active();
						var year_d = cot_y.get_value();
						if(month == 0)
						{
							month = 12;
							year_d--;
						}
						if(month < 10)
						{
							cot_s = @"$year_d-0$month";
						}
						else
						{
							cot_s = @"$year_d-$month";
						}	
					}								
					db.exec(@"INSERT INTO altas (cc, nombre, apellido1, apellido2, fnacimiento, telefono, email, dir, zona, obs, ocu, sec, centro, dirc, obsc, afiliado, cotizado) VALUES ('$cc_s', '$name_s', '$surname1_s', '$surname2_s', '$birth_s', '$phone_s', '$mail_s', '$address_s', '$zone_s', '$obs_s', '$ocu_s', '$sec_s', '$center_s', '$address_c_s', '$obs_c_s', '$date_s', '$cot_s')");
					afiliado = true;
					break;
				}
				else
				{
					adj.set_value(0);
					mensaje_pop(cc,"Ya existe este CC en la base de datos.");
				}
			}
			else
			{
				adj.set_value(0);
				mensaje_pop(cc,"Es necesario un CC.");
			}
		}
		else
		{
			break;
		}
	}
	dialog.destroy();
	return (afiliado);
}

bool cotizar(Gtk.ApplicationWindow window, Gtk.TreeSelection selection, int table_active, Gtk.ListStore list_af)
{
	var settings = new Settings ("cnt.tesoreria");
	var sett_cot = settings.get_double ("cotizacion");
		
	bool cotizado = false;
	Gtk.Dialog dialog = new Gtk.Dialog.with_buttons("Cotizar", window, Gtk.DialogFlags.MODAL, "Cancelar", Gtk.ResponseType.CANCEL, "Aceptar", Gtk.ResponseType.OK);
	//dialog.set_resizable(false);
	var content_area = dialog.get_content_area();
	content_area.set_property("margin", 10);
	content_area.set_property("spacing", 10);
	
	Gtk.Grid grid = new Gtk.Grid();
	grid.set_column_spacing(10);
	grid.set_row_spacing(10);
	
	Gtk.TreeModel model;
    Gtk.TreeIter iter;
	Gtk.TreeIter iter_pre;
	Value cc;
	selection.get_selected(out model, out iter);
	model.get_value (iter, 1, out cc);
    iter_pre = iter;
	var cc_s = (string) cc;
	Gtk.Label label_cc = new Gtk.Label(cc_s);
	label_cc.set_markup(@"<big>$(label_cc.get_text())</big>");
	grid.attach(label_cc, 0, 0, 2, 1);
	
	Gtk.Label label_quan = new Gtk.Label("Cotización mensual (€):");
	label_quan.halign = Gtk.Align.END;
	grid.attach(label_quan, 0, 1, 1, 1);
	Gtk.SpinButton quantity = new Gtk.SpinButton.with_range(0,999,1);
	quantity.set_digits(2);
	quantity.set_value(sett_cot);
	grid.attach(quantity, 1, 1, 1, 1);	
	
	Gtk.Label label_des = new Gtk.Label("Ingresado en caja:");
	label_des.halign = Gtk.Align.END;
	grid.attach(label_des, 0, 2, 1, 1);
	Gtk.ListStore list = new Gtk.ListStore(1, typeof(string));
	if (cajas > 0)
	{
		db.exec(@"SELECT name FROM sqlite_master WHERE name != 'altas' AND name != 'bajas' AND name != 'cotizaciones'", (n_columns, values, column_names) => 
		{
			list.append(out iter);
			list.set(iter, 0, values[0]);
			return 0;
		}, null);
	} 
	Gtk.ComboBox combo = new Gtk.ComboBox.with_model(list);
	Gtk.CellRendererText cell = new Gtk.CellRendererText ();
	combo.pack_start(cell,false);
	combo.set_attributes (cell, "text", 0);
	combo.set_active(-1);
	grid.attach(combo, 1, 2, 1, 1);	
	
	Gtk.Label label_months = new Gtk.Label("Meses:");
	label_months.halign = Gtk.Align.END;
	grid.attach(label_months, 0, 3, 1, 1);
	Gtk.SpinButton months = new Gtk.SpinButton.with_range(1,60,1);
	grid.attach(months, 1, 3, 1, 1);
    
    Gtk.Label label_date = new Gtk.Label("Fecha del movimiento:");
	label_date.halign = Gtk.Align.END;
	grid.attach(label_date, 0, 4, 1, 1);
	Gtk.Entry date = new Gtk.Entry();
	date.max_length = 10;
	date.set_property("width-chars", 12);
	date.set_placeholder_text("dd/mm/aaaa");
	date.set_icon_from_icon_name(0, "x-office-calendar-symbolic");		
	date.icon_press.connect (() =>
	{
		date.grab_focus();
		calendario(date);
	});
    texto_desde_hasta(date, false);
    grid.attach(date, 1, 4, 1, 1);
	
	content_area.add(grid);
	content_area.show_all();
	combo.grab_focus();
	
	while(true)
	{
		var result = dialog.run();
		if(result == Gtk.ResponseType.OK) 
		{
			if(combo.get_active() != -1)
			{
				combo.get_active_iter(out iter);
				model = combo.get_model();
				Value name_cash;
				model.get_value(iter, 0, out name_cash);
				var name_cash_s = (string) name_cash;
				int months_i = months.get_value_as_int();
				var quantity_i = quantity.get_value();
				string concept_s;
				if (months_i == 1)
				{
					concept_s = @"$cc_s cotiza 1 mes";
				}
				else
				{
					concept_s = @"$cc_s cotiza $months_i meses";
					quantity_i = months_i * quantity_i;
				}
                string time = formatea_texto(date, 2);
                if(time != "")
                {
                    db.exec(@"INSERT INTO $name_cash_s (fecha, factura, concepto, euros) VALUES ('$time', '', '$concept_s', $quantity_i)");
                
                    string table;
                    if(table_active == 0)
                    {
                        table = "altas";    
                    }
                    else
                    {
                        table = "bajas";
                    }
                    db.exec(@"SELECT cotizado FROM $table WHERE cc = '$cc_s'", (n_columns, values, column_names) => 
                    {
                        var year_pre = int.parse(values[0][0:4]);
                        var month_pre = int.parse(values[0][5:7]);
                        var year = year_pre;
                        var month = month_pre + months_i;
                        while(month > 12)
                        {
                            month = month -12;
                            year++;
                        }
                        string cot_s;
                        if(month < 10)
                        {
                            cot_s = @"$year-0$month";
                        }
                        else
                        {
                            cot_s = @"$year-$month";
                        }	
                        db.exec(@"UPDATE $table SET cotizado='$cot_s' WHERE cc='$cc_s'");
                        string year_s = cot_s[0:4];
                        string month_s = mes_texto(int.parse(cot_s[5:7]));
                        list_af.set_value(iter_pre, 17, @"$month_s $year_s");
					
                        if(month_pre == 12)
                        {
                            year_pre++;
                            month_pre = 1;
                        }
                        else
                        {
                            month_pre++;
                        }
                        while(months_i > 0)
                        {
                            bool exist = false;			
                            db.exec(@"SELECT anho FROM cotizaciones WHERE anho = '$year_pre'", (n_columns, values, column_names) => 
                            {
                                exist = true;
                                return 0;					
                            }, null);
                            if (exist == false)
                            {
                                db.exec(@"INSERT INTO cotizaciones (anho, enero, febrero, marzo, abril, mayo, junio, julio, agosto, septiembre, octubre, noviembre, diciembre) VALUES ('$year_pre', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0')");
                            }
                            while(month_pre < 13 && months_i >0)
                            {
                                if(month_pre == 1)
                                {
                                    db.exec(@"UPDATE cotizaciones SET enero=enero+1 WHERE anho='$year_pre'");
                                }
                                else if (month_pre == 2)
                                {
                                    db.exec(@"UPDATE cotizaciones SET febrero=febrero+1 WHERE anho='$year_pre'");
                                }
                                else if (month_pre == 3)
                                {
                                    db.exec(@"UPDATE cotizaciones SET marzo=marzo+1 WHERE anho='$year_pre'");
                                }							
                                else if (month_pre == 4)
                                {
                                    db.exec(@"UPDATE cotizaciones SET abril=abril+1 WHERE anho='$year_pre'");
                                }
                                else if (month_pre == 5)
                                {
                                    db.exec(@"UPDATE cotizaciones SET mayo=mayo+1 WHERE anho='$year_pre'");
                                }	
                                else if (month_pre == 6)
                                {
                                    db.exec(@"UPDATE cotizaciones SET junio=junio+1 WHERE anho='$year_pre'");
                                }
                                else if (month_pre == 7)
                                {
                                    db.exec(@"UPDATE cotizaciones SET julio=julio+1 WHERE anho='$year_pre'");
                                }							
                                else if (month_pre == 8)
                                {
                                    db.exec(@"UPDATE cotizaciones SET agosto=agosto+1 WHERE anho='$year_pre'");
                                }
                                else if (month_pre == 9)
                                {
                                    db.exec(@"UPDATE cotizaciones SET septiembre=septiembre+1 WHERE anho='$year_pre'");
                                }	
                                else if (month_pre == 10)
                                {
                                    db.exec(@"UPDATE cotizaciones SET octubre=octubre+1 WHERE anho='$year_pre'");
                                }
                                else if (month_pre == 11)
                                {
                                    db.exec(@"UPDATE cotizaciones SET noviembre=noviembre+1 WHERE anho='$year_pre'");
                                }							
                                else if (month_pre == 12)
                                {
                                    db.exec(@"UPDATE cotizaciones SET diciembre=diciembre+1 WHERE anho='$year_pre'");
                                }							
                                month_pre++;
                                months_i--;
                            }
                            year_pre++;
                            month_pre = 1;
                        }
                        return 0;					
                    }, null);	
				
                    cotizado = true;
                    settings.set_double ("cotizacion", quantity.get_value());
                    break;
                }
                else
                {
                    mensaje_pop(date, "Fecha incorrecta.");
                }  
			}
			else
			{
				mensaje_pop(combo, "Es necesario seleccionar una opción.");
			}
		}
		else
		{
			break;
		}
	}
	dialog.destroy();	
	return cotizado;
}

void editar(Gtk.ApplicationWindow window, Gtk.TreeSelection selection, int table_active, Gtk.ListStore list_af)
{
	Gtk.TreeModel model;
	Gtk.TreeIter iter_pre;
	Value cc_v;
	selection.get_selected(out model, out iter_pre);
	model.get_value (iter_pre, 1, out cc_v);
	var cc_pre = (string) cc_v;	
	string name_pre = "";
	string surname1_pre = "";
	string surname2_pre = "";
	string birth_pre = "";
	string phone_pre = "";
	string mail_pre = "";
	string address_pre = "";
	string zone_pre = "";
	string obs_pre = "";
	string ocu_pre = "";
	string sec_pre = "";
	string center_pre = "";
	string address_c_pre = "";
	string obs_c_pre = "";
	string date_pre = "";
	string cot_pre = "";
	string date_out_pre = "";
	
	string table;
    if(table_active == 0)
    {
		table = "altas";    
    }
    else
    {
		table = "bajas";
    }	
	
	db.exec(@"SELECT * FROM $table WHERE cc = '$cc_pre' ", (n_columns, values, column_names) => 
	{
		name_pre = values[1];
		surname1_pre = values[2];
		surname2_pre = values[3];
		birth_pre = values[4];
		phone_pre = values[5];
		mail_pre = values[6];
		address_pre = values[7];
		zone_pre = values[8];
		obs_pre = values[9];
		ocu_pre = values[10];
		sec_pre = values[11];
		center_pre = values[12];
		address_c_pre = values[13];
		obs_c_pre = values[14];
		date_pre = values[15];
		cot_pre = values[16];
		if(table == "bajas")
		{
			date_out_pre = values[17];
		}
		return 0;
	}, null);	
	
	Gtk.Dialog dialog = new Gtk.Dialog.with_buttons("Editar", window, Gtk.DialogFlags.MODAL, "Cancelar", Gtk.ResponseType.CANCEL, "Aceptar", Gtk.ResponseType.OK);
	dialog.set_resizable(false);
	var content_area = dialog.get_content_area();
	content_area.set_property("margin", 10);
	content_area.set_property("spacing", 10);
	dialog.set_size_request (450, 500);
	
	Gtk.Grid grid = new Gtk.Grid();
	grid.set_column_spacing(10);
	grid.set_row_spacing(10);
	
	Gtk.Label label_name = new Gtk.Label("Nombre:");
	label_name.halign = Gtk.Align.END;
	grid.attach(label_name, 0, 0, 1, 1);
	Gtk.Entry name = new Gtk.Entry();
	name.max_length = 25;
	name.set_text(name_pre);
	grid.attach(name, 1, 0, 1, 1);
	
	Gtk.Label label_surname1 = new Gtk.Label("Primer apellido:");
	label_surname1.halign = Gtk.Align.END;
	grid.attach(label_surname1, 0, 1, 1, 1);
	Gtk.Entry surname1 = new Gtk.Entry();
	surname1.max_length = 25;
	surname1.set_text(surname1_pre);
	grid.attach(surname1, 1, 1, 1, 1);
	
	Gtk.Label label_surname2 = new Gtk.Label("Segundo apellido:");
	label_surname2.halign = Gtk.Align.END;
	grid.attach(label_surname2, 0, 2, 1, 1);
	Gtk.Entry surname2 = new Gtk.Entry();
	surname2.max_length = 25;
	surname2.set_text(surname2_pre);
	grid.attach(surname2, 1, 2, 1, 1);	
	
	Gtk.Label label_birth = new Gtk.Label("Fecha de nacimiento:");
	label_birth.halign = Gtk.Align.END;
	grid.attach(label_birth, 0, 3, 1, 1);
	Gtk.Entry birth = new Gtk.Entry();
	birth.max_length = 10;
	birth.set_property("width-chars", 12);
	birth.set_placeholder_text("dd/mm/aaaa");
	birth.set_icon_from_icon_name(0, "x-office-calendar-symbolic");			
	birth.icon_press.connect (() =>
	{
		birth.grab_focus();
		calendario(birth);
	});
	birth.set_text(birth_pre);
	grid.attach(birth, 1, 3, 1, 1);
	
	Gtk.Label label_ocu = new Gtk.Label("Ocupación:");
	label_ocu.halign = Gtk.Align.END;
	grid.attach(label_ocu, 0, 4, 1, 1);
	Gtk.Entry ocu = new Gtk.Entry();
	autocompletado(ocu, "ocu");
	ocu.max_length = 50;
	ocu.set_text(ocu_pre);
	grid.attach(ocu, 1, 4, 1, 1);	
	
	Gtk.Label label_sec = new Gtk.Label("Sección:");
	label_sec.halign = Gtk.Align.END;
	grid.attach(label_sec, 0, 5, 1, 1);
	Gtk.Entry sec = new Gtk.Entry();
	autocompletado(sec, "sec");
	sec.max_length = 50;
	sec.set_text(sec_pre);
	grid.attach(sec, 1, 5, 1, 1);	
	
	Gtk.Label label_cc = new Gtk.Label("Carné Confederal:");
	label_cc.halign = Gtk.Align.END;
	grid.attach(label_cc, 0, 6, 1, 1);
	Gtk.Entry cc = new Gtk.Entry();
	cc.max_length = 20;
	cc.set_text(cc_pre);
	cc.set_sensitive(false);
	grid.attach(cc, 1, 6, 1, 1);		
	
	Gtk.Label label_mail = new Gtk.Label("e-mail:");
	label_mail.halign = Gtk.Align.END;
	grid.attach(label_mail, 0, 7, 1, 1);
	Gtk.Entry mail = new Gtk.Entry();
	mail.max_length = 50;
	mail.set_text(mail_pre);
	grid.attach(mail, 1, 7, 1, 1);	
	
	Gtk.Label label_phone = new Gtk.Label("Teléfono:");
	label_phone.halign = Gtk.Align.END;
	grid.attach(label_phone, 0, 8, 1, 1);
	Gtk.Entry phone = new Gtk.Entry();
	phone.max_length = 25;
	phone.set_text(phone_pre);
	grid.attach(phone, 1, 8, 1, 1);	
	
	Gtk.Label label_address = new Gtk.Label("Dirección:");
	label_address.halign = Gtk.Align.END;
	grid.attach(label_address, 0, 9, 1, 1);
	Gtk.Entry address = new Gtk.Entry();
	address.max_length = 100;
	address.set_text(address_pre);
	grid.attach(address, 1, 9, 1, 1);	
	
	Gtk.Label label_zone = new Gtk.Label("Localidad/Barrio:");
	label_zone.halign = Gtk.Align.END;
	grid.attach(label_zone, 0, 10, 1, 1);
	Gtk.Entry zone = new Gtk.Entry();
	autocompletado(zone, "zona");
	zone.max_length = 50;
	zone.set_text(zone_pre);
	grid.attach(zone, 1, 10, 1, 1);	
	
	Gtk.Label label_obs = new Gtk.Label("Observaciones:");
	label_obs.halign = Gtk.Align.END;
	grid.attach(label_obs, 0, 11, 1, 1);
	Gtk.Entry obs = new Gtk.Entry();
	obs.max_length = 140;
	obs.set_text(obs_pre);
	grid.attach(obs, 1, 11, 1, 1);		
	
	Gtk.Label label_center = new Gtk.Label("Centro de trabajo:");
	label_center.halign = Gtk.Align.END;
	grid.attach(label_center, 0, 12, 1, 1);
	Gtk.Entry center = new Gtk.Entry();
	autocompletado(center, "centro");
	center.max_length = 50;
	center.set_text(center_pre);
	grid.attach(center, 1, 12, 1, 1);	
	
	Gtk.Label label_address_c = new Gtk.Label("Dirección del centro:");
	label_address_c.halign = Gtk.Align.END;
	grid.attach(label_address_c, 0, 13, 1, 1);
	Gtk.Entry address_c = new Gtk.Entry();
	autocompletado(address_c, "dirc");
	address_c.max_length = 100;
	address_c.set_text(address_c_pre);
	grid.attach(address_c, 1, 13, 1, 1);	
	
	Gtk.Label label_obs_c = new Gtk.Label("Observaciones del centro:");
	label_obs_c.halign = Gtk.Align.END;
	grid.attach(label_obs_c, 0, 14, 1, 1);
	Gtk.Entry obs_c = new Gtk.Entry();
	autocompletado(obs_c, "obsc");
	obs_c.max_length = 100;
	obs_c.set_text(obs_c_pre);
	grid.attach(obs_c, 1, 14, 1, 1);	
	
	Gtk.Label label_date = new Gtk.Label("Fecha de afiliación:");
	label_date.halign = Gtk.Align.END;
	grid.attach(label_date, 0, 15, 1, 1);
	Gtk.Entry date = new Gtk.Entry();
	date.max_length = 10;
	date.set_property("width-chars", 12);
	date.set_placeholder_text("dd/mm/aaaa");
	date.set_icon_from_icon_name(0, "x-office-calendar-symbolic");		
	date.icon_press.connect (() =>
	{
		date.grab_focus();
		calendario(date);
	});
	date.set_text(date_pre);
	grid.attach(date, 1, 15, 1, 1);	
	
	Gtk.Label label_exempt = new Gtk.Label("Exento/a:");
	label_exempt.halign = Gtk.Align.END;
	grid.attach(label_exempt, 0, 16, 1, 1);
	Gtk.CheckButton exempt = new Gtk.CheckButton();
	if(cot_pre == "exento/a")
	{
		exempt.set_active(true);
	}
	grid.attach(exempt, 1, 16, 1, 1);	
	
	Gtk.Box hbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);	
	Gtk.Label label_cot = new Gtk.Label("Próxima cotización:");
	label_cot.halign = Gtk.Align.END;
	grid.attach(label_cot, 0, 17, 1, 1);
	Gtk.ListStore list = new Gtk.ListStore(1, typeof(string));
	Gtk.TreeIter iter;
	list.append(out iter);
	list.set(iter, 0, "Enero");
	list.append(out iter);
	list.set(iter, 0, "Febrero");	
	list.append(out iter);
	list.set(iter, 0, "Marzo");
	list.append(out iter);
	list.set(iter, 0, "Abril");
	list.append(out iter);
	list.set(iter, 0, "Mayo");
	list.append(out iter);
	list.set(iter, 0, "Junio");	
	list.append(out iter);
	list.set(iter, 0, "Julio");	
	list.append(out iter);
	list.set(iter, 0, "Agosto");
	list.append(out iter);
	list.set(iter, 0, "Septiembre");
	list.append(out iter);
	list.set(iter, 0, "Octubre");
	list.append(out iter);
	list.set(iter, 0, "Noviembre");	
	list.append(out iter);
	list.set(iter, 0, "Diciembre");					
	Gtk.ComboBox combo_month = new Gtk.ComboBox.with_model(list);
	Gtk.CellRendererText cell = new Gtk.CellRendererText ();
	combo_month.pack_start(cell,false);
	combo_month.set_attributes (cell, "text", 0);	
	hbox.add(combo_month);
	Gtk.SpinButton cot_y = new Gtk.SpinButton.with_range(1910,2999,1);
	int month;
	int year;
	if(cot_pre == "exento/a")
	{
		combo_month.set_active(6);
		combo_month.set_sensitive(false);
		cot_y.set_value(1936);
		cot_y.set_sensitive(false);
	}
	else
	{
		month = int.parse(cot_pre[5:7]);
		year = int.parse(cot_pre[0:4]);
		if (month == 12)
		{
				month = 0;
				year++;
		}
		combo_month.set_active(month);
		cot_y.set_value(year);
	}
	hbox.add(cot_y);
	grid.attach(hbox, 1, 17, 1, 1);	
	
	Gtk.Label label_date_out = new Gtk.Label("Fecha baja:");
	label_date_out.halign = Gtk.Align.END;
	grid.attach(label_date_out, 0, 18, 1, 1);
	Gtk.Entry date_out = new Gtk.Entry();
	date_out.max_length = 10;
	date_out.set_property("width-chars", 12);
	date_out.set_placeholder_text("dd/mm/aaaa");
	date_out.set_icon_from_icon_name(0, "x-office-calendar-symbolic");		
	date_out.icon_press.connect (() =>
	{
		date_out.grab_focus();
		calendario(date_out);
	});
	date_out.set_text(date_out_pre);
	grid.attach(date_out, 1, 18, 1, 1);			
	
	Gtk.ScrolledWindow scroll = new Gtk.ScrolledWindow(null,null);		
	scroll.set_propagate_natural_width(true);
	scroll.add(grid);	
	content_area.pack_start(scroll, true, true, 0);
	content_area.show_all();
	if(table == "altas")
	{
		label_date_out.set_visible(false);	
		date_out.set_visible(false);
	}	
	
	exempt.clicked.connect ( () =>
	{
		var sens = combo_month.get_sensitive();
		combo_month.set_sensitive(!sens);
		cot_y.set_sensitive(!sens);	
	});
	
	while(true)
	{
		var result = dialog.run();
		if(result == Gtk.ResponseType.OK) 
		{
			string name_s = name.get_text();
			string surname1_s = surname1.get_text();
			string surname2_s = surname2.get_text();
			string birth_s = birth.get_text();
			string ocu_s = ocu.get_text();
			string sec_s = sec.get_text();
			string mail_s = mail.get_text();
			string phone_s = phone.get_text();
			string address_s = address.get_text();
			string zone_s = zone.get_text();
			string obs_s = obs.get_text();
			string center_s = center.get_text();
			string address_c_s = address_c.get_text();	
			string obs_c_s = obs_c.get_text();	
			string date_s = date.get_text();
			string cot_s;	
			if(exempt.get_active() == true)
			{
				cot_s = "exento/a"; 
			}
			else
			{
				month = combo_month.get_active();
				var year_d = cot_y.get_value();
				if(month == 0)
				{
					month = 12;
					year_d--;
				}
				if(month < 10)
				{
					cot_s = @"$year_d-0$month";
				}
				else
				{
					cot_s = @"$year_d-$month";
				}	
			}
			string date_out_s = "";
			if(table == "bajas")
			{
				date_out_s = date_out.get_text();
				db.exec(@"UPDATE bajas SET nombre='$name_s', apellido1='$surname1_s', apellido2='$surname2_s', fnacimiento='$birth_s', telefono='$phone_s', email='$mail_s', dir='$address_s', zona='$zone_s', obs='$obs_s', ocu='$ocu_s', sec='$sec_s', centro='$center_s', dirc='$address_c_s', obsc='$obs_c_s', afiliado='$date_s', cotizado='$cot_s', fechabaja='$date_out_s' WHERE cc='$cc_pre'");
			}								
			else
			{	
				db.exec(@"UPDATE altas SET nombre='$name_s', apellido1='$surname1_s', apellido2='$surname2_s', fnacimiento='$birth_s', telefono='$phone_s', email='$mail_s', dir='$address_s', zona='$zone_s', obs='$obs_s', ocu='$ocu_s', sec='$sec_s', centro='$center_s', dirc='$address_c_s', obsc='$obs_c_s', afiliado='$date_s', cotizado='$cot_s' WHERE cc='$cc_pre'");						
			}	
            if(cot_s != "exento/a")
            {
                string year_s = cot_s[0:4];
                string month_s = mes_texto(int.parse(cot_s[5:7]));
                cot_s = @"$month_s $year_s";
            }
            list_af.set(iter_pre, 2, surname1_s, 3, surname2_s, 4, name_s, 5, birth_s, 6, phone_s, 7, mail_s, 8, address_s, 9, zone_s, 10, obs_s, 11, ocu_s, 12, sec_s, 13, center_s, 14, address_c_s, 15, obs_c_s, 16, date_s, 17, cot_s, 18, date_out_s);
			break;
		}
		else
		{
			break;
		}
	}
	dialog.destroy();
}

void editar_cc(Gtk.ApplicationWindow window, Gtk.TreeSelection selection, int table_active, Gtk.ListStore list_af)
{
	Gtk.TreeModel model;
	Gtk.TreeIter iter_pre;
	Value cc_v;
	selection.get_selected(out model, out iter_pre);
	model.get_value (iter_pre, 1, out cc_v);
	var cc_pre = (string) cc_v;	
	
	string table;
    if(table_active == 0)
    {
		table = "altas";    
    }
    else
    {
		table = "bajas";
    }
	
	string obs_pre = "";
	db.exec(@"SELECT obs FROM $table WHERE cc = '$cc_pre' ", (n_columns, values, column_names) => 
	{
		obs_pre = values[0];
		return 0;
	}, null);	
	
	Gtk.Dialog dialog = new Gtk.Dialog.with_buttons("Editar CC", window, Gtk.DialogFlags.MODAL, "Cancelar", Gtk.ResponseType.CANCEL, "Aceptar", Gtk.ResponseType.OK);
	dialog.set_resizable(false);
	var content_area = dialog.get_content_area();
	content_area.set_property("margin", 10);
	content_area.set_property("spacing", 10);
	
	Gtk.Grid grid = new Gtk.Grid();
	grid.set_column_spacing(10);
	grid.set_row_spacing(10);
	
	Gtk.Label label_cc_pre = new Gtk.Label("Carné Confederal antiguo:");
	label_cc_pre.halign = Gtk.Align.END;
	grid.attach(label_cc_pre, 0, 0, 1, 1);
	Gtk.Entry cc_entry_pre = new Gtk.Entry();
	cc_entry_pre.set_text(cc_pre);
	cc_entry_pre.set_sensitive(false);
	grid.attach(cc_entry_pre, 1, 0, 1, 1);	
	
	Gtk.Label label_cc = new Gtk.Label("Carné Confederal nuevo:");
	label_cc.halign = Gtk.Align.END;
	grid.attach(label_cc, 0, 1, 1, 1);
	Gtk.Entry cc = new Gtk.Entry();
	cc.max_length = 20;
	grid.attach(cc, 1, 1, 1, 1);
	
	content_area.add(grid);
	content_area.show_all();
	
	while(true)
	{
		var result = dialog.run();
		if(result == Gtk.ResponseType.OK) 
		{
			if(cc.get_text() != "")
			{
				var exist = false;
				db.exec("SELECT cc FROM altas", (n_columns, values, column_names) => 
				{
					if(values[0] == cc.get_text())
					{
						exist = true;
					}
					return 0;
				}, null);
				db.exec("SELECT cc FROM bajas", (n_columns, values, column_names) => 
				{
					if(values[0] == cc.get_text())
					{
						exist = true;
					}
					return 0;
				}, null);						
				if(exist == false)
				{
					string cc_s = cc.get_text();
					string obs = @"C.C. antiguo: $cc_pre. $obs_pre";			
					db.exec(@"UPDATE $table SET cc='$cc_s', obs='$obs' WHERE cc='$cc_pre'");
                    string num = "";
                    if(cc_s.char_count() > 3)
                    {
                        string aux = @"$cc_s ";
                        num = aux[-5:-1];
                    }
                    list_af.set(iter_pre, 0, num, 1, cc_s, 10, obs);				
					break;
				}
				else
				{
					mensaje_pop(cc,"Ya existe este CC en la base de datos.");
				}
			}
			else
			{
				mensaje_pop(cc,"Es necesario un CC.");
			}
		}
		else
		{
			break;
		}
	}
	dialog.destroy();
}

void causar_baja(Gtk.ApplicationWindow window, Gtk.TreeSelection selection, Gtk.ListStore list_af, Gtk.Label rows_label)
{	
	Gtk.Dialog dialog = new Gtk.Dialog.with_buttons("Causar baja", window, Gtk.DialogFlags.MODAL, "Cancelar", Gtk.ResponseType.CANCEL, "Aceptar", Gtk.ResponseType.OK);
	//dialog.set_default_size(300, 300);
	//dialog.set_resizable(false);
	var content_area = dialog.get_content_area();
	content_area.set_property("margin", 10);
	content_area.set_property("spacing", 10);
	
	Gtk.Grid grid = new Gtk.Grid();
	grid.set_column_spacing(10);
	grid.set_row_spacing(10);
		
	Gtk.Label label_out = new Gtk.Label("Carné baja:");
	label_out.halign = Gtk.Align.END;
	grid.attach(label_out, 0, 0, 1, 1);
	Gtk.TreeModel model;
	Gtk.TreeIter iter;
	Value cc;
	selection.get_selected(out model, out iter);
	model.get_value (iter, 1, out cc);
	var cc_s = (string) cc;
	Gtk.Label label_cc = new Gtk.Label(cc_s);
	grid.attach(label_cc, 1, 0, 1, 1);
	
	Gtk.Label label_date = new Gtk.Label("Fecha baja:");
	label_date.halign = Gtk.Align.END;
	grid.attach(label_date, 0, 1, 1, 1);
	Gtk.Entry date = new Gtk.Entry();
	date.max_length = 10;
	date.set_property("width-chars", 12);
	date.set_placeholder_text("dd/mm/aaaa");
	date.set_icon_from_icon_name(0, "x-office-calendar-symbolic");	
	texto_desde_hasta(date, false);		
	date.icon_press.connect (() =>
	{
		date.grab_focus();
		calendario(date);
	});
	grid.attach(date, 1, 1, 1, 1);		

	content_area.add(grid);
	content_area.show_all();
	
	var result = dialog.run();
	if(result == Gtk.ResponseType.OK) 
	{			
		db.exec(@"SELECT * FROM altas WHERE cc = '$cc_s' ", (n_columns, values, column_names) => 
		{
			string name = values[1];
			string surname1 = values[2];
			string surname2 = values[3];
			string birth = values[4];
			string phone = values[5];
			string email = values[6];
			string address = values[7];
			string zone = values[8];
			string obs = values[9];
			string ocu = values[10];
			string sec = values[11];
			string center = values[12];
			string address_c = values[13];
			string obs_c = values[14];
			string date_up = values[15];
			string cot = values[16];
			string date_out = date.get_text();
			db.exec(@"INSERT INTO bajas (cc, nombre, apellido1, apellido2, fnacimiento, telefono, email, dir, zona, obs, ocu, sec, centro, dirc, obsc, afiliado, cotizado, fechabaja) VALUES ('$cc_s','$name','$surname1','$surname2','$birth','$phone','$email','$address','$zone','$obs','$ocu','$sec','$center','$address_c','$obs_c','$date_up','$cot','$date_out')");
			return 0;
		}, null);	
		db.exec(@"DELETE FROM altas WHERE cc = '$cc_s'");
        list_af.remove(ref iter);
        string rows_s = rows_label.get_text();
        int rows = int.parse(rows_s) - 1;
        if(rows == 1)
        {
            rows_label.set_text("1 fila");
        }
        else
        {
            rows_label.set_text(@"$rows filas");
        }
	}
	dialog.destroy();
}

void reincorporar_baja(Gtk.ApplicationWindow window, Gtk.TreeSelection selection, Gtk.ListStore list_af, Gtk.Label rows_label)
{
	Gtk.Dialog dialog = new Gtk.Dialog.with_buttons("Reincorporar baja", window, Gtk.DialogFlags.MODAL, "Cancelar", Gtk.ResponseType.CANCEL, "Aceptar", Gtk.ResponseType.OK);
	dialog.set_resizable(false);
	var content_area = dialog.get_content_area();
	content_area.set_property("margin", 10);
	content_area.set_property("spacing", 10);
	
	Gtk.Grid grid = new Gtk.Grid();
	grid.set_column_spacing(10);
	grid.set_row_spacing(10);
		
	Gtk.Label label_out = new Gtk.Label("Carné a reincorporar:");
	label_out.halign = Gtk.Align.END;
	grid.attach(label_out, 0, 0, 1, 1);
	Gtk.TreeModel model;
	Gtk.TreeIter iter;
	Value cc;
	selection.get_selected(out model, out iter);
	model.get_value (iter, 1, out cc);
	var cc_s = (string) cc;
	Gtk.Label label_cc = new Gtk.Label(cc_s);
	grid.attach(label_cc, 1, 0, 1, 1);

	content_area.add(grid);
	content_area.show_all();
	
	var result = dialog.run();
	if(result == Gtk.ResponseType.OK) 
	{			
		db.exec(@"SELECT * FROM bajas WHERE cc = '$cc_s' ", (n_columns, values, column_names) => 
		{
			string name = values[1];
			string surname1 = values[2];
			string surname2 = values[3];
			string birth = values[4];
			string phone = values[5];
			string email = values[6];
			string address = values[7];
			string zone = values[8];
			string obs = values[9];
			string ocu = values[10];
			string sec = values[11];
			string center = values[12];
			string address_c = values[13];
			string obs_c = values[14];
			string date_up = values[15];
			string cot = values[16];
			db.exec(@"INSERT INTO altas (cc, nombre, apellido1, apellido2, fnacimiento, telefono, email, dir, zona, obs, ocu, sec, centro, dirc, obsc, afiliado, cotizado) VALUES ('$cc_s','$name','$surname1','$surname2','$birth','$phone','$email','$address','$zone','$obs','$ocu','$sec','$center','$address_c','$obs_c','$date_up','$cot')");
			return 0;
		}, null);	
		db.exec(@"DELETE FROM bajas WHERE cc = '$cc_s'");
        list_af.remove(ref iter);
        string rows_s = rows_label.get_text();
        int rows = int.parse(rows_s) - 1;
        if(rows == 1)
        {
            rows_label.set_text("1 fila");
        }
        else
        {
            rows_label.set_text(@"$rows filas");
        }     
	}
	dialog.destroy();
}

void eliminar_baja(Gtk.ApplicationWindow window, Gtk.TreeSelection selection, Gtk.ListStore list_af, Gtk.Label rows_label)
{
	Gtk.Dialog dialog = new Gtk.Dialog.with_buttons("Eliminar baja", window, Gtk.DialogFlags.MODAL, "Cancelar", Gtk.ResponseType.CANCEL, "Aceptar", Gtk.ResponseType.OK);
	dialog.set_resizable(false);
	var content_area = dialog.get_content_area();
	content_area.set_property("margin", 10);
	content_area.set_property("spacing", 10);
	
	Gtk.Grid grid = new Gtk.Grid();
	grid.set_column_spacing(10);
	grid.set_row_spacing(10);
		
	Gtk.Label label_out = new Gtk.Label("Carné a eliminar:");
	label_out.halign = Gtk.Align.END;
	grid.attach(label_out, 0, 0, 1, 1);
	Gtk.TreeModel model;
	Gtk.TreeIter iter;
	Value cc;
	selection.get_selected(out model, out iter);
	model.get_value (iter, 1, out cc);
	var cc_s = (string) cc;
	Gtk.Label label_cc = new Gtk.Label(cc_s);
	grid.attach(label_cc, 1, 0, 1, 1);

	content_area.add(grid);
	content_area.show_all();
	
	var result = dialog.run();
	if(result == Gtk.ResponseType.OK) 
	{				
		db.exec(@"DELETE FROM bajas WHERE cc = '$cc_s'");
        list_af.remove(ref iter);
        string rows_s = rows_label.get_text();
        int rows = int.parse(rows_s) - 1;
        if(rows == 1)
        {
            rows_label.set_text("1 fila");
        }
        else
        {
            rows_label.set_text(@"$rows filas");
        }      
	}
	dialog.destroy();
}

void mensaje_pop(Gtk.Widget widget, string str)
{
	// Se crea un menú y se le asigna a la entrada
	Gtk.Popover pop = new Gtk.Popover(widget);
				
	// Se crea una caja y se añade al menú
	Gtk.Box boxpop = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
	boxpop.set_property("margin", 10);
	pop.add(boxpop);
				
	//Se añaden elementos a la caja del menú
	Gtk.Label label = new Gtk.Label(str);
	boxpop.pack_start(label);
				
	// Se muestra el pop
	pop.show_all();
}

void autocompletado (Gtk.Entry entry, string col)
{
	Gtk.EntryCompletion completion = new Gtk.EntryCompletion();
	entry.set_completion(completion);
	Gtk.ListStore list = new Gtk.ListStore(1, typeof(string));
	completion.set_model(list);
	completion.set_text_column(0);
	Gtk.TreeIter iter;
	list.append(out iter);
	db.exec(@"SELECT DISTINCT $col FROM altas", (n_columns, values, column_names) => 
	{
		list.set(iter, 0, values[0]);
		list.append(out iter);
		return 0;
	}, null);	
}

void aux_exportar_af(bool[] checks, string[] values, string table, DataOutputStream dos)
{
	string num = "";
	string date_out = "";
	string cc = values[0];
	string name = values[1];
	string surname1 = values[2];
	string surname2 = values[3];
	string birth = values[4];
	string phone = values[5];
	string mail = values[6];
	string address = values[7];
	string zone = values[8];
	string obs = values[9];
	string ocu = values[10];
	string sec = values[11];
	string center = values[12];
	string address_c = values[13];
	string obs_c = values[14];
	string date = values[15];
	string cot_s = values[16];
	if(cot_s != "exento/a")
	{
		string year = cot_s[0:4];
		string month = mes_texto(int.parse(cot_s[5:7]));
		cot_s = @"$month $year";
	}
	if(cc.char_count() > 3)
	{
		string aux = @"$cc ";
		num = aux[-5:-1];
	}
	if(table == "bajas")
	{
		date_out = values[17];			
	}			
	try
	{
		if(checks[0] == true)
		{
			dos.put_string(@"$num;");
		}
		if(checks[1] == true)
		{
			dos.put_string(@"$cc;");
		}
		if(checks[2] == true)
		{
			dos.put_string(@"$name;");
		}
		if(checks[3] == true)
		{
			dos.put_string(@"$surname1;");
		}
		if(checks[4] == true)
		{
			dos.put_string(@"$surname2;");
		}
		if(checks[5] == true)
		{
			dos.put_string(@"$birth;");
		}	
		if(checks[6] == true)
		{
			dos.put_string(@"$phone;");
		}	
		if(checks[7] == true)
		{
			dos.put_string(@"$mail;");
		}
		if(checks[8] == true)
		{
			dos.put_string(@"$address;");
		}
		if(checks[9] == true)
		{
			dos.put_string(@"$zone;");
		}
		if(checks[10] == true)
		{
			dos.put_string(@"$obs;");
		}
		if(checks[11] == true)
		{
			dos.put_string(@"$ocu;");
		}
		if(checks[12] == true)
		{
			dos.put_string(@"$sec;");
		}	
		if(checks[13] == true)
		{
			dos.put_string(@"$center;");
		}
		if(checks[14] == true)
		{
			dos.put_string(@"$address_c;");
		}
		if(checks[15] == true)
		{
			dos.put_string(@"$obs_c;");
		}
		if(checks[16] == true)
		{
			dos.put_string(@"$date;");
		}
		if(checks[17] == true)
		{
			dos.put_string(@"$cot_s;");
		}
		if(checks[18] == true)
		{
			dos.put_string(@"$date_out;");
		}	
		dos.put_string("\n");																	
	}
	catch(Error e2)
	{
		stderr.printf("%s\n", e2.message);
	}
}

bool[] genera_checks (DataOutputStream dos)
{    
	// Se toman del schema los valores de los check
	var settings = new Settings ("cnt.tesoreria.mostrar");
	bool checks[19];
	checks[0] = settings.get_boolean ("numero");
	checks[1] = settings.get_boolean ("cc");
	checks[2] = settings.get_boolean ("nombre");
	checks[3] = settings.get_boolean ("apellido1");
	checks[4] = settings.get_boolean ("apellido2");
	checks[5] = settings.get_boolean ("fnacimiento");
	checks[6] = settings.get_boolean ("telefono");
	checks[7] = settings.get_boolean ("email");
	checks[8] = settings.get_boolean ("dir");
	checks[9] = settings.get_boolean ("zona");
	checks[10] = settings.get_boolean ("obs");
	checks[11] = settings.get_boolean ("ocu");
	checks[12] = settings.get_boolean ("sec");
	checks[13] = settings.get_boolean ("centro");
	checks[14] = settings.get_boolean ("dirc");
	checks[15] = settings.get_boolean ("obsc");
	checks[16] = settings.get_boolean ("afiliado");
	checks[17] = settings.get_boolean ("cotizado");
	checks[18] = settings.get_boolean ("fechabaja");

	try
	{
		if(checks[0] == true)
		{
			dos.put_string("Número;");
		}
		if(checks[1] == true)
		{
			dos.put_string("CC;");
		}
		if(checks[2] == true)
		{
			dos.put_string("Nombre;");
		}
		if(checks[3] == true)
		{
			dos.put_string("Primer apellido;");
		}
		if(checks[4] == true)
		{
			dos.put_string("Segundo apellido;");
		}
		if(checks[5] == true)
		{
			dos.put_string("Fecha de nacimiento;");
		}	
		if(checks[6] == true)
		{
			dos.put_string("Teléfono;");
		}	
		if(checks[7] == true)
		{
			dos.put_string("e-mail;");
		}
		if(checks[8] == true)
		{
			dos.put_string("Dirección;");
		}
		if(checks[9] == true)
		{
			dos.put_string("Localidad/Barrio;");
		}
		if(checks[10] == true)
		{
			dos.put_string("Observaciones;");
		}
		if(checks[11] == true)
		{
			dos.put_string("Ocupación;");
		}
		if(checks[12] == true)
		{
			dos.put_string("Sección;");
		}	
		if(checks[13] == true)
		{
			dos.put_string("Centro de trabajo;");
		}
		if(checks[14] == true)
		{
			dos.put_string("Dirección centro;");
		}
		if(checks[15] == true)
		{
			dos.put_string("Observaciones centro;");
		}
		if(checks[16] == true)
		{
			dos.put_string("Fecha afiliación;");
		}
		if(checks[17] == true)
		{
			dos.put_string("Mes cotizado;");
		}
		if(checks[18] == true)
		{
			dos.put_string("Fecha baja;");
		}	
		dos.put_string("\n");																	
	}
	catch(Error e1)
	{
		stderr.printf("%s\n", e1.message);
	}  
    return checks;
}
