void activate (Gtk.Application app) 
{	
	// Se conecta la base de datos
	conectar_db();
		
	// Se crea y posiciona la ventana de la aplicación
	var settings = new GLib.Settings ("cnt.tesoreria");
	var width = settings.get_int ("ventana-anchura");
	var height = settings.get_int ("ventana-altura");
	var x = settings.get_int ("ventana-x");
	var y = settings.get_int ("ventana-y");
	Gtk.ApplicationWindow window = new Gtk.ApplicationWindow (app);
	window.set_default_size (width, height);
	window.set_gravity(Gdk.Gravity.NORTH_WEST);
	window.move(x,y);
			
	// Se elige un icono por defecto (no solo para esta ventana)
	try
	{
		var icon = new Gdk.Pixbuf.from_file("/usr/share/icons/tesoreria.png");
		Gtk.Window.set_default_icon(icon);
	}
	catch(GLib.Error e)
	{
		stdout.printf("%s\n",e.message);
	}

	// Se crea la barra de cabecera y se coloca como título de la ventana
	Gtk.HeaderBar headerbar = new Gtk.HeaderBar();
	headerbar.show_close_button = true;
	window.set_titlebar(headerbar);
    
    // Botón auxiliar (no dibujado) se usa solo para comunicar desde preferencias a cajas y afiliación	
	Gtk.Button aux_button2 = new Gtk.Button();
			
	// Se crea un botón de menú, se le asigna un popover y se coloca al final de la barra de cabecera
	Gtk.MenuButton menubutton = new Gtk.MenuButton();
	Gtk.Popover pop = new Gtk.Popover(menubutton);		
	menubutton.set_popover(pop);
	crea_menu(window, pop, aux_button2);
	Gtk.Image menuimage = new Gtk.Image.from_icon_name("open-menu-symbolic", Gtk.IconSize.MENU);
	menubutton.add(menuimage);
	menubutton.set_tooltip_text("Menú");
	headerbar.pack_end(menubutton);
	menubutton.clicked.connect (() =>{pop.show_all();});
	
	// Se crea un botón de búsqueda, se le añade una barra de búsqueda y se coloca al final de la barra de cabecera	
	Gtk.SearchBar bar = new Gtk.SearchBar();
	Gtk.SearchEntry searchentry = new Gtk.SearchEntry();
	bar.connect_entry(searchentry);
	bar.add(searchentry);
	bar.set_search_mode(false);
	Gtk.ToggleButton searchbutton = new Gtk.ToggleButton();
	Gtk.Image serchimage = new Gtk.Image.from_icon_name("edit-find-symbolic", Gtk.IconSize.MENU);
	searchbutton.add(serchimage);
	searchbutton.set_tooltip_text("Búsqueda");
	headerbar.pack_end(searchbutton);		

	// Se crea el stack
	Gtk.Stack stack = new Gtk.Stack();
	stack.set_border_width(10);
	
	// Botón auxiliar (no dibujado) se usa solo para comunicar desde afiliados a cajas al cotizar	
	Gtk.Button aux_button = new Gtk.Button();

	// Se crean elementos del stack (1)
	artilugios_af(stack, window, aux_button, aux_button2, searchentry);
		
	// Se crean elementos del stack (2)
	if(cajas > 0)
	{
		artilugios_ca(stack, window, aux_button, aux_button2, searchentry);
	}
	else
	{
		artilugios_ca_prev(stack, window, aux_button, aux_button2, searchentry);
	}
    
	// Se crea un intercambiador para el stack
	Gtk.StackSwitcher stack_switcher = new Gtk.StackSwitcher();
	stack_switcher.halign = Gtk.Align.CENTER;
	stack_switcher.set_stack(stack);
		
	// Se coloca el intercambiador como título de la barra de cabecera
	headerbar.set_custom_title (stack_switcher);
			
	// Se crea una caja y se posiciona dentro el stack
	Gtk.Box vbox = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
		
	// Se añade la barra de búsqueda a la caja
	vbox.add(bar);
	
	// Se añade el stack a la caja
	vbox.pack_start(stack, true, true, 0);
			
	// Se añade la caja a la ventana
	window.add(vbox);
			
	// Se muestran todos los elementos de la ventana
	window.show_all ();
    
    // Se toma del schema el stack visible y se selecciona
    var sett_switch = settings.get_string ("switch");
    stack.set_visible_child_name(sett_switch);
    
    // Se muestra menú opciones o botones individuales
    aux_button2.clicked();
	
	//Se comprueban las fechas de los últimos registros en las cajas respecto a la del sistema
	if (cajas > 0)
	{
		bool futuro = fecha();
		if (futuro == true)
		{
			dialogo_fecha(window);
		}
	}
    
    // Se entra al pulsar el botón de búsqueda
	searchbutton.toggled.connect (() =>
	{
		var active = searchbutton.get_active();
		
		// Cierra la barra de búsqueda
		if(active == false)
		{
			bar.set_search_mode(false);
		}
		
		// Abre la barra de búsqueda
		else
		{
			bar.set_search_mode(true);
		}
	});
	
	// Se entra al recibir cualquier evento	
	window.key_press_event.connect((event) =>
	{
		var keyval = event.keyval;
		var mod = Gtk.accelerator_get_label(keyval, event.state);
		
		// Ctrl+F abre la barra de búsqueda
		if (mod == "Ctrl+F" || mod == "Ctrl+Mod2+F")
		{
			var active = searchbutton.get_active();
			if(active == true)
			{
				searchbutton.set_active(false);
			}
			else
			{
				searchbutton.set_active(true);
			}
			return true;
		}
		// Si se teclea algo se manda el evento a la barra
		return bar.handle_event(event);
	});	
	
    // Si se abre o cierra la barra
    bar.notify["search-mode-enabled"].connect (() =>
	{
        // Se llama a la siguiente función
        on_search_mode_enabled(bar, searchbutton);
    });	
    
	// Se ejecuta al cerrar la ventana para guardar su estado
	window.delete_event.connect((event) =>
	{
		window.get_size (out x, out y);
		settings.set_int ("ventana-altura", y);
		settings.set_int ("ventana-anchura", x);
		window.get_position (out x, out y);
		settings.set_int ("ventana-x", x);
		settings.set_int ("ventana-y", y);
        sett_switch = stack.get_visible_child_name();
        settings.set_string("switch", sett_switch);
		return false;
	});	
}

void on_search_mode_enabled(Gtk.SearchBar bar, Gtk.ToggleButton searchbutton)
{
    // Si la barra está activa, se activa el botón de búsqueda
    if (bar.search_mode_enabled) 
    {
        searchbutton.set_active(true);
    }
    // Si no, se desactiva el botón de búsqueda
    else
    {
        searchbutton.set_active(false);
    }
}

int main (string[] args) 
{
	Gtk.Application app = new Gtk.Application("CNT.tesoreria", ApplicationFlags.FLAGS_NONE);
	app.activate.connect(() => {activate(app);});
	return app.run (args);
}
