void calendario (Gtk.Entry date)
{
	// Se crea un menú asignándolo a date
	Gtk.Popover pop_calendar = new Gtk.Popover(date);
				
	// Se crea una caja y se añade al menú
	Gtk.Box boxpop_calendar = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
	pop_calendar.add(boxpop_calendar);
				
	//Se añade el calendario a la caja del menú
	Gtk.Calendar calendar = new Gtk.Calendar();
	boxpop_calendar.pack_start(calendar);
	
	//Se inicializa el calendario con la fecha del texto
	texto_a_calendario(date, calendar);
				
	// Se muestra el menú
	pop_calendar.show_all();
			
	// Se pasa a texto la selección del calendario
	calendar.day_selected_double_click.connect ( () =>
	{
		uint y, m, d;
		calendar.get_date(out y, out m, out d);
		var time = new DateTime.local((int)y,(int)m+1,(int)d,0,0,0);
		var date_text = time.format("%d/%m/%Y");
		date.set_text(date_text);
		pop_calendar.hide();
	});	
}

void texto_desde_hasta (Gtk.Entry date, bool desde)
{
	var now = new DateTime.now_local();
	string date_s;
	if(desde == true)
	{
		var prev = now.add_months(-1);
		date_s = prev.format("%d/%m/%Y");
	}
	else
	{
		date_s = now.format("%d/%m/%Y");
	}
	date.set_text(date_s);
}

void texto_a_calendario (Gtk.Entry date, Gtk.Calendar calendar)
{
	string texto = date.get_text();
	if(texto.char_count() == 10)
	{
		string day = texto[0:2];
		string month = texto[3:5];
		string year = texto[6:10];
		int d = int.parse(day);
		int m = int.parse(month);
		int y = int.parse(year);
		
		bool digit = false;
		string digits = @"$day$month$year";
		for(int i=0; i<digits.char_count(); i++)
		{
			digit = digits[i].isdigit();
			if(digit == false)
			{
				break;
			}
		}
		
		if(digit == true && texto[2] == '/' && texto[5] == '/' && d>0 && d<32 && m>0 && m<13 && y>999) //Se comprueba que la entrada sea igual al formato
		{
			//Creando un DateTime se evita que se le pasen valores erróneos al calendario
			var time = new DateTime.local(y,m,d,0,0,0);
			time.get_ymd(out y, out m, out d);
			
			calendar.set_property("day", d);
			calendar.set_property("month", m-1);
			calendar.set_property("year", y);
		}
	}
}

string formatea_texto(Gtk.Entry entry, int since_to_now)
{
	string time_s = "";
	string texto = entry.get_text();
	if(texto.char_count() == 10)
	{
		string day = texto[0:2];
		string month = texto[3:5];
		string year = texto[6:10];
		int d = int.parse(day);
		int m = int.parse(month);
		int y = int.parse(year);
		
		bool digit = false;
		string digits = @"$day$month$year";
		for(int i=0; i<digits.char_count(); i++)
		{
			digit = digits[i].isdigit();
			if(digit == false)
			{
				break;
			}
		}
		
		if(digit == true && texto[2] == '/' && texto[5] == '/' && d>0 && d<32 && m>0 && m<13 && y>999) //Se comprueba que la entrada sea igual al formato
		{
			if(since_to_now == 0) //since: 0 
			{
				//Creando un DateTime se evita que se le pasen valores erróneos
				var time = new DateTime.local(y,m,d,0,0,0);
				time_s = time.format("%Y-%m-%d %H-%M-%S");
			}
			else if(since_to_now == 1) //to: 1
			{
				//Creando un DateTime se evita que se le pasen valores erróneos
				var time = new DateTime.local(y,m,d,23,59,59);
				time_s = time.format("%Y-%m-%d %H-%M-%S");
			}
            else // now: 2
            {
                var now = new DateTime.now_local();
                var hour = now.get_hour();
                var minute =  now.get_minute();
                var second = now.get_second();
                //Creando un DateTime se evita que se le pasen valores erróneos
				var time = new DateTime.local(y,m,d,hour,minute,second);
				time_s = time.format("%Y-%m-%d %H-%M-%S");
            } 
		}
	}
	return time_s;
}

string formatea_texto_esp(Gtk.Entry entry)
{
	string time_s = "";
	string texto = entry.get_text();
	if(texto.char_count() == 10)
	{
		string day = texto[0:2];
		string month = texto[3:5];
		string year = texto[6:10];
		int d = int.parse(day);
		int m = int.parse(month);
		int y = int.parse(year);

		//Creando un DateTime se evita que se le pasen valores erróneos
		var time = new DateTime.local(y,m,d,0,0,0);
		time_s = time.format("%d/%m/%Y");
	}
	return time_s;	
}

string mes_texto (int month)
{
	string month_s = "";
	if(month == 1)
	{
		month_s = "Enero";
	}
	else if(month == 2)
	{
		month_s = "Febrero";
	}
	else if(month == 3)
	{
		month_s = "Marzo";
	}
	else if(month == 4)
	{
		month_s = "Abril";
	}
	else if(month == 5)
	{
		month_s = "Mayo";
	}
	else if(month == 6)
	{
		month_s = "Junio";
	}
	else if(month == 7)
	{
		month_s = "Julio";
	}
	else if(month == 8)
	{
		month_s = "Agosto";
	}
	else if(month == 9)
	{
		month_s = "Septiembre";
	}	
	else if(month == 10)
	{
		month_s = "Octubre";
	}
	else if(month == 11)
	{
		month_s = "Noviembre";
	}
	else if(month == 12)
	{
		month_s = "Diciembre";
	}	
	return month_s;		
}

bool fecha()
{
	bool futuro = false;
	string[] nombres = new string[cajas];
	int cnt = 0;
	db.exec("SELECT name FROM sqlite_master WHERE name != 'altas' AND name != 'bajas' AND name != 'cotizaciones'", (n_columns, values, column_names) => 
    {
		nombres[cnt] = values[0];
		cnt++;
		return 0;
    }, null);
    var now = new DateTime.now_local();
	var time = now.format("%Y-%m-%d %H-%M-%S");
    for (int i=0; i < cnt; i++)
    {
		string nombre = nombres[i];
		db.exec(@"SELECT fecha FROM $nombre ORDER BY fecha DESC LIMIT 1", (n_columns, values, column_names) => 
		{
			if(time < values[0])
			{
				futuro = true;
			}
			return 0;
		}, null);
	}
	return futuro;
}

void dialogo_fecha (Gtk.ApplicationWindow window)
{
	Gtk.MessageDialog dialog = new Gtk.MessageDialog(window, Gtk.DialogFlags.MODAL, Gtk.MessageType.WARNING, Gtk.ButtonsType.CLOSE, "La fecha del último registro es posterior a la actual.\n\nComprueba que la hora del sistema es correcta.");
	dialog.run();
	dialog.destroy();
}
